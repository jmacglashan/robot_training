package tools;

import burlap.behavior.singleagent.vfa.DifferentiableStateActionValue;
import burlap.behavior.singleagent.vfa.common.LinearFVVFA;
import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.Domain;
import burlap.oomdp.singleagent.GroundedAction;
import experiments.turtlebot.vfa.FVAndBoundConstant;
import experiments.turtlebot.vfa.TileCodedVision;
import org.yaml.snakeyaml.Yaml;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * @author James MacGlashan.
 */
public class VFASerialization {


	public static void serialize(DifferentiableStateActionValue vfa, String path){
		Map<AbstractGroundedAction, Integer> offsets = null;
		if(vfa instanceof LinearFVVFA) {
			offsets = ((LinearFVVFA)vfa).getActionOffset();
		}
		else if(vfa instanceof FVAndBoundConstant){
			offsets = ((FVAndBoundConstant)vfa).getActionOffsets();
		}
		else if(vfa instanceof TileCodedVision){
			offsets = ((TileCodedVision)vfa).getActionOffsets();
		}
		Map<String, Integer> noffsets = new HashMap<String, Integer>(offsets.size());
		for(Map.Entry<AbstractGroundedAction, Integer> e : offsets.entrySet()){
			noffsets.put(e.getKey().toString(), e.getValue());
		}
		double [] weights = new double[vfa.numParameters()];
		for(int i = 0; i < weights.length; i++){
			weights[i] = vfa.getParameter(i);
		}

		SerializedStateActionWeights s = new SerializedStateActionWeights(noffsets, weights);
		Yaml yaml = new Yaml();
		try {
			yaml.dump(s, new BufferedWriter(new FileWriter(path)));
		} catch(IOException e) {
			e.printStackTrace();
		}

	}

	public static void deseriailize(DifferentiableStateActionValue vfa, String path, Domain domain){

		Yaml yaml = new Yaml();
		SerializedStateActionWeights s = null;
		try {
			s = (SerializedStateActionWeights)yaml.load(new FileReader(path));
		} catch(FileNotFoundException e) {
			e.printStackTrace();
		}

		if(s != null) {

			for(Map.Entry<String, Integer> e : s.offsets.entrySet()) {
				setActionOffset(vfa, domain, e);
			}

			if(vfa.numParameters() == 0){
				if(vfa instanceof LinearFVVFA){
					((LinearFVVFA)vfa).initializeStateActionWeightVector(s.weights.length, 0.);
				}
				else if(vfa instanceof FVAndBoundConstant){
					((FVAndBoundConstant)vfa).initializeStateActionWeightVector(s.weights.length, 0.);
				}

			}

			for(int i = 0; i < s.weights.length; i++) {
				vfa.setParameter(i, s.weights[i]);
			}

		}

	}

	protected static void setActionOffset(DifferentiableStateActionValue vfa, Domain domain, Map.Entry<String, Integer> e){
		GroundedAction ga = domain.getAction(e.getKey()).getAssociatedGroundedAction();
		if(vfa instanceof LinearFVVFA){
			((LinearFVVFA)vfa).setActionOffset(ga, e.getValue());
		}
		else if(vfa instanceof FVAndBoundConstant){
			((FVAndBoundConstant)vfa).setActionOffset(ga, e.getValue());
		}
		else if(vfa instanceof TileCodedVision){
			((TileCodedVision)vfa).setActionOffset(ga, e.getValue());
		}
	}


	public static class SerializedStateActionWeights{

		public Map<String, Integer> offsets;
		public double [] weights;

		public SerializedStateActionWeights() {
		}

		public SerializedStateActionWeights(Map<String, Integer> offsets, double[] weights) {
			this.offsets = offsets;
			this.weights = weights;
		}
	}

}
