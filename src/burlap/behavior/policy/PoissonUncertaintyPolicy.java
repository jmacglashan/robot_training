package burlap.behavior.policy;

import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.states.State;
import org.apache.commons.math3.distribution.PoissonDistribution;

import java.util.List;

/**
 * @author James MacGlashan.
 */
public class PoissonUncertaintyPolicy extends Policy{

	protected Policy delegate;
	protected double maxConfidence;
	protected double minConfidence;
	protected double lambdaScale;
	protected AbstractGroundedAction lastSelection;
	protected int timeToSelect = 0;

	public PoissonUncertaintyPolicy(Policy delegate, double maxConfidence, double minConfidence, double lambdaScale) {
		this.delegate = delegate;
		this.maxConfidence = maxConfidence;
		this.minConfidence = minConfidence;
		this.lambdaScale = lambdaScale;
	}

	@Override
	public AbstractGroundedAction getAction(State s) {

		if(this.timeToSelect == 0){

			this.lastSelection = this.delegate.getAction(s);

			double maxProb = this.maxProb(s);
			if(maxProb < this.maxConfidence){
				double scale = 1 - ((maxProb - minConfidence) / (maxConfidence - minConfidence));
				double lambda = scale*this.lambdaScale;
				PoissonDistribution pd = new PoissonDistribution(lambda);
				this.timeToSelect = pd.sample();
			}

		}
		else{
			this.timeToSelect--;
		}

		return lastSelection;
	}

	@Override
	public List<ActionProb> getActionDistributionForState(State s) {
		return this.delegate.getActionDistributionForState(s);
	}

	protected double maxProb(State s){
		List<ActionProb> aps = this.delegate.getActionDistributionForState(s);
		double max = 0.;
		for(ActionProb ap : aps){
			max = Math.max(ap.pSelection, max);
		}

		return max;
	}

	@Override
	public boolean isStochastic() {
		return this.delegate.isStochastic();
	}

	@Override
	public boolean isDefinedFor(State s) {
		return this.delegate.isDefinedFor(s);
	}
}
