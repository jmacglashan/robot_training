package burlap.behavior.policy;

import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.states.State;
import org.apache.commons.math3.distribution.PoissonDistribution;

import java.util.List;

/**
 * @author James MacGlashan.
 */
public class PoissonSelectionPolicy extends Policy{

	protected Policy delegate;
	protected double lambda;
	protected AbstractGroundedAction lastSelection;
	protected int timeToSelect = 0;

	protected PoissonDistribution pd;

	public PoissonSelectionPolicy(Policy delegate, double lambda) {
		this.delegate = delegate;
		this.lambda = lambda;
		this.pd = new PoissonDistribution(lambda);
	}

	@Override
	public AbstractGroundedAction getAction(State s) {

		if(timeToSelect == 0){
			this.lastSelection = this.delegate.getAction(s);
			timeToSelect = pd.sample();
		}
		else{
			timeToSelect--;
		}

		return lastSelection;
	}

	@Override
	public List<ActionProb> getActionDistributionForState(State s) {
		return this.delegate.getActionDistributionForState(s);
		//throw new UnsupportedOperationException("PoissonSelectionPolicy is non-stationary and cannot return action probability distributions");
	}

	@Override
	public boolean isStochastic() {
		return true;
	}

	@Override
	public boolean isDefinedFor(State s) {
		return this.delegate.isDefinedFor(s);
	}


}
