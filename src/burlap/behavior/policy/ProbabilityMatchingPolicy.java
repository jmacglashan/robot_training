package burlap.behavior.policy;

import burlap.behavior.singleagent.MDPSolverInterface;
import burlap.behavior.valuefunction.QFunction;
import burlap.behavior.valuefunction.QValue;
import burlap.debugtools.RandomFactory;
import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.states.State;

import java.util.ArrayList;
import java.util.List;

/**
 * @author James MacGlashan.
 */
public class ProbabilityMatchingPolicy extends Policy implements SolverDerivedPolicy{

	protected QFunction qSource;
	protected double incomeBase = 1.;

	public ProbabilityMatchingPolicy() {
	}

	public ProbabilityMatchingPolicy(QFunction qSource) {
		this.qSource = qSource;
	}

	public ProbabilityMatchingPolicy(QFunction qSource, double incomeBase) {
		this.qSource = qSource;
		this.incomeBase = incomeBase;
	}

	public ProbabilityMatchingPolicy(double incomeBase) {
		this.incomeBase = incomeBase;
	}

	@Override
	public void setSolver(MDPSolverInterface solver) {
		if(!(solver instanceof QFunction)){
			throw new RuntimeException("ProbabilityMatchingPolicy cannot have its solver set to the input, because it does not implement QFunction.");
		}
		this.qSource = (QFunction)solver;
	}

	@Override
	public AbstractGroundedAction getAction(State s) {

		List<QValue> qs = this.qSource.getQs(s);
		double min = Math.min(this.minIncome(qs), 0.);
		double sum = this.sumIncome(qs, min);

		if(sum == 0.){
			int select = RandomFactory.getMapped(0).nextInt(qs.size());
			return qs.get(select).a;
		}

		double roll = RandomFactory.getMapped(0).nextDouble();
		double psum = 0.;
		for(QValue q : qs){
			double p = (q.q - min + this.incomeBase) / sum;
			psum += p;
			if(roll < psum){
				return q.a;
			}
		}

		throw new RuntimeException("Could not return action, because probability did not sum to 1, it summed to " + psum);
	}

	@Override
	public List<ActionProb> getActionDistributionForState(State s) {

		List<QValue> qs = this.qSource.getQs(s);
		double min = Math.min(this.minIncome(qs), 0.);
		double sum = this.sumIncome(qs, min);

		List<ActionProb> aps = new ArrayList<ActionProb>(qs.size());

		if(sum == 0.){
			double uni = 1. / qs.size();
			for(QValue q : qs){
				aps.add(new ActionProb(q.a, uni));
			}
			return aps;
		}

		double psum = 0.;
		for(QValue q : qs) {
			double p = (q.q - min + this.incomeBase) / sum;
			psum += p;
			aps.add(new ActionProb(q.a, p));
		}

		if(Math.abs(psum - 1.) > 1e-16) {
			throw new RuntimeException("Policy distribution did not sum to 1, it summed to " + psum);
		}

		return aps;
	}

	@Override
	public boolean isStochastic() {
		return true;
	}

	@Override
	public boolean isDefinedFor(State s) {
		return qSource.getQs(s).size() > 0;
	}

	protected double sumIncome(List<QValue> qs, double min){
		double sum = incomeBase * qs.size();
		for(QValue q : qs){
			sum += (q.q - min);
		}
		return sum;
	}

	protected double minIncome(List<QValue> qs){
		double min = Double.POSITIVE_INFINITY;

		for(QValue q : qs){
			min = Math.min(q.q, min);
		}

		return min;
	}
}
