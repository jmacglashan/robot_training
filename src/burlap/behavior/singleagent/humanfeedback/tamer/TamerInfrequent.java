package burlap.behavior.singleagent.humanfeedback.tamer;

import burlap.behavior.policy.GreedyQPolicy;
import burlap.behavior.policy.Policy;
import burlap.behavior.singleagent.EpisodeAnalysis;
import burlap.behavior.singleagent.MDPSolver;
import burlap.behavior.singleagent.humanfeedback.FeedbackReceiver;
import burlap.behavior.singleagent.humanfeedback.tamer.rewardmodel.HumanRewardModel;
import burlap.behavior.singleagent.learning.LearningAgent;
import burlap.behavior.valuefunction.QFunction;
import burlap.behavior.valuefunction.QValue;
import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.Domain;
import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.GroundedAction;
import burlap.oomdp.singleagent.environment.Environment;
import burlap.oomdp.singleagent.environment.EnvironmentOutcome;

import java.util.ArrayList;
import java.util.List;

/**
 * An implementation of TAMER with infrequent action selection [1] (when the the trainer has enough time to explicitly provide
 * feedback for each state-action pair). When human feedback of zero is given, it is ignored and not used as a training sample.
 *
 * <br/><br/>
 * 1. W. Bradley Knox. Learning from Human-Generated Reward. Dissertation. September 2012.
 * @author James MacGlashan.
 */
public class TamerInfrequent extends MDPSolver implements LearningAgent, FeedbackReceiver, QFunction{

	protected HumanRewardModel hr;
	protected volatile double lastHumanReward;
	protected Policy learningPolicy = new GreedyQPolicy(this);

	public TamerInfrequent(Domain domain, HumanRewardModel hr) {
		this.solverInit(domain, hr, null, 0., null);
		this.hr = hr;
	}

	public HumanRewardModel getHr() {
		return hr;
	}

	public void setHr(HumanRewardModel hr) {
		this.hr = hr;
	}

	public Policy getLearningPolicy() {
		return learningPolicy;
	}

	public void setLearningPolicy(Policy learningPolicy) {
		this.learningPolicy = learningPolicy;
	}


	@Override
	public void receiveHumanFeedback(double f) {
		lastHumanReward += f;
	}

	@Override
	public EpisodeAnalysis runLearningEpisode(Environment env) {
		return this.runLearningEpisode(env, -1);
	}

	@Override
	public EpisodeAnalysis runLearningEpisode(Environment env, int maxSteps) {
		this.lastHumanReward = 0.;
		EpisodeAnalysis ea = new EpisodeAnalysis(env.getCurrentObservation());

		int steps = 0;
		while(!env.isInTerminalState() && (steps < maxSteps || maxSteps == -1) ){

			State curState = env.getCurrentObservation();
			GroundedAction ga = (GroundedAction)learningPolicy.getAction(curState);
			EnvironmentOutcome eo = ga.executeIn(env);
			double h = this.getAndResetHumanFeedback();
			ea.recordTransitionTo(ga, eo.op, h);
			if(h != 0.) {
				this.hr.updateModel(curState, ga, h);
			}

			steps++;

		}

		return ea;
	}

	@Override
	public void resetSolver() {
		this.lastHumanReward = 0.;
		this.hr.resetModel();
	}

	@Override
	public List<QValue> getQs(State s) {
		List<GroundedAction> gas = this.getAllGroundedActions(s);
		List<QValue> qs = new ArrayList<QValue>(gas.size());
		for(GroundedAction ga : gas){
			qs.add(this.getQ(s, ga));
		}
		return qs;
	}

	@Override
	public QValue getQ(State s, AbstractGroundedAction a) {
		QValue q = new QValue(s, a, this.hr.reward(s, (GroundedAction)a, null));
		return q;
	}

	@Override
	public double value(State s) {
		return QFunctionHelper.getOptimalValue(this, s);
	}

	protected double getAndResetHumanFeedback(){
		double val = this.lastHumanReward;
		this.lastHumanReward = 0.;
		return val;
	}

}
