package burlap.behavior.singleagent.humanfeedback.tamer.rewardmodel;

import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.GroundedAction;
import burlap.oomdp.singleagent.RewardFunction;

/**
 * @author James MacGlashan.
 */
public interface HumanRewardModel extends RewardFunction{

	void updateModel(State s, GroundedAction ga, double reward);
	HumanRewardModel copy();
	void resetModel();


}
