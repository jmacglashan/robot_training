package burlap.behavior.singleagent.humanfeedback.ilearning;

import burlap.behavior.policy.BoltzmannQPolicy;
import burlap.behavior.policy.Policy;
import burlap.behavior.singleagent.EpisodeAnalysis;
import burlap.behavior.singleagent.MDPSolver;
import burlap.behavior.singleagent.humanfeedback.FeedbackReceiver;
import burlap.behavior.singleagent.learning.LearningAgent;
import burlap.behavior.singleagent.vfa.*;
import burlap.behavior.valuefunction.QFunction;
import burlap.behavior.valuefunction.QValue;
import burlap.behavior.valuefunction.ValueFunctionInitialization;
import burlap.datastructures.HashedAggregator;
import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.Domain;
import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.GroundedAction;
import burlap.oomdp.singleagent.environment.Environment;
import burlap.oomdp.singleagent.environment.EnvironmentOutcome;

import java.util.*;

/**
 * @author James MacGlashan.
 */
public class ILearningApproximation extends MDPSolver implements LearningAgent, QFunction, FeedbackReceiver {


	protected DifferentiableStateActionValue				vfa;

	protected ValueFunctionInitialization 					incomeInitialization = new ValueFunctionInitialization.ConstantValueFunctionInitialization(0.);

	protected Policy 										learningPolicy;

	protected FeedbackAndDiscount							lastFeedback = new FeedbackAndDiscount();

	protected FeedbackDiscountFunction discountFunction;

	protected boolean										useReplacingTraces= true;

	protected boolean 										useEnvironmentFeedback = true;

	protected double										learningRate = 1.;


	public ILearningApproximation(Domain domain, double discount, DifferentiableStateActionValue vfa, double boltzTemp) {
		this.solverInit(domain, null, null, discount, null);
		this.vfa = vfa;
		this.learningPolicy = new BoltzmannQPolicy(this, boltzTemp);
		this.discountFunction = new FeedbackDiscountFunction.ConstantFeedbackDiscountFunction(discount);
	}

	public ILearningApproximation(Domain domain, double discount, DifferentiableStateActionValue vfa, double boltzTemp, boolean useEnvironmentFeedback) {
		this.solverInit(domain, null, null, discount, null);
		this.vfa = vfa;
		this.learningPolicy = new BoltzmannQPolicy(this, boltzTemp);
		this.discountFunction = new FeedbackDiscountFunction.ConstantFeedbackDiscountFunction(discount);
		this.useEnvironmentFeedback = useEnvironmentFeedback;
	}

	@Override
	synchronized public void receiveHumanFeedback(double f) {
		this.lastFeedback.feedback += f;
		double discount = this.discountFunction.discountForFeedback(f);
		if(discount > this.lastFeedback.discount){
			this.lastFeedback.discount = discount;
		}
	}

	public Policy getLearningPolicy() {
		return learningPolicy;
	}

	public void setLearningPolicy(Policy learningPolicy) {
		this.learningPolicy = learningPolicy;
	}

	public FeedbackDiscountFunction getDiscountFunction() {
		return discountFunction;
	}

	public void setDiscountFunction(FeedbackDiscountFunction discountFunction) {
		this.discountFunction = discountFunction;
	}

	public boolean isUseReplacingTraces() {
		return useReplacingTraces;
	}

	public void setUseReplacingTraces(boolean useReplacingTraces) {
		this.useReplacingTraces = useReplacingTraces;
	}

	public boolean isUseEnvironmentFeedback() {
		return useEnvironmentFeedback;
	}

	public void setUseEnvironmentFeedback(boolean useEnvironmentFeedback) {
		this.useEnvironmentFeedback = useEnvironmentFeedback;
	}

	public double getLearningRate() {
		return learningRate;
	}

	public void setLearningRate(double learningRate) {
		this.learningRate = learningRate;
	}

	@Override
	public EpisodeAnalysis runLearningEpisode(Environment env) {
		return this.runLearningEpisode(env, -1);
	}

	@Override
	public EpisodeAnalysis runLearningEpisode(Environment env, int maxSteps) {
		EpisodeAnalysis ea = new EpisodeAnalysis(env.getCurrentObservation());
		int steps = 0;
		while(!env.isInTerminalState() && (steps < maxSteps || maxSteps == -1)){

			GroundedAction ga = (GroundedAction)this.learningPolicy.getAction(env.getCurrentObservation());
			EnvironmentOutcome eo = ga.executeIn(env);
			ea.recordTransitionTo(ga, eo.op, eo.r);
			FeedbackAndDiscount f = this.getAndResetFeedback(eo);
			if(f.feedback != 0.){
				this.updateIncome(ea, f);
			}

			steps++;

		}

		return ea;
	}

	protected synchronized FeedbackAndDiscount getAndResetFeedback(EnvironmentOutcome eo){

		if(this.useEnvironmentFeedback){
			return new FeedbackAndDiscount(eo.r, this.gamma);
		}

		FeedbackAndDiscount toRet = new FeedbackAndDiscount(this.lastFeedback);
		this.lastFeedback.reset();

		return toRet;
	}

	protected void updateIncome(EpisodeAnalysis ea, FeedbackAndDiscount feedback){

		HashedAggregator<Integer> weightChanges = new HashedAggregator<Integer>();


		for(int t = ea.maxTimeStep()-1; t >= 0; t--){

			double pred = this.vfa.evaluate(ea.getState(t), ea.getAction(t));
			FunctionGradient gradient = this.vfa.gradient(ea.getState(t), ea.getAction(t));
			for(FunctionGradient.PartialDerivative pd : gradient.getNonZeroPartialDerivatives()){
				if(!this.useReplacingTraces || !weightChanges.containsKey(pd.parameterId)){
					double delta = pd.value * feedback.feedback;
					weightChanges.add(pd.parameterId, delta);
				}
			}


			feedback.decrement();
		}

		for(Map.Entry<Integer, Double> e : weightChanges.entrySet()){
			double nw = this.vfa.getParameter(e.getKey()) + (this.learningRate * e.getValue());
			this.vfa.setParameter(e.getKey(), nw);
		}

	}


	@Override
	public void resetSolver() {
		this.vfa.resetParameters();
	}

	@Override
	public List<QValue> getQs(State s) {
		List<GroundedAction> actions = this.getAllGroundedActions(s);
		List<QValue> qs = new ArrayList<QValue>(actions.size());
		for(GroundedAction ga : actions){
			qs.add(new QValue(s, ga, this.vfa.evaluate(s, ga)));
		}

		return qs;
	}

	@Override
	public QValue getQ(State s, AbstractGroundedAction a) {
		return new QValue(s, a, this.vfa.evaluate(s, a));
	}

	@Override
	public double value(State s) {
		return QFunctionHelper.getOptimalValue(this, s);
	}


}
