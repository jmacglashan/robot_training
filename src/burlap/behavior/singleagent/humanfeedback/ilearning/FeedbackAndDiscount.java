package burlap.behavior.singleagent.humanfeedback.ilearning;

/**
* @author James MacGlashan.
*/
public class FeedbackAndDiscount {
	double feedback = 0.;
	double discount = 0.;

	public FeedbackAndDiscount() {
	}

	public FeedbackAndDiscount(double feedback, double discount) {
		this.feedback = feedback;
		this.discount = discount;
	}

	public FeedbackAndDiscount(FeedbackAndDiscount fd) {
		this.feedback = fd.feedback;
		this.discount = fd.discount;
	}

	public void reset(){
		this.feedback = 0.;
		this.discount = 0.;
	}

	public void decrement(){
		this.feedback *= this.discount;
	}

}
