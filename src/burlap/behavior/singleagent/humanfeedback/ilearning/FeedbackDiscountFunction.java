package burlap.behavior.singleagent.humanfeedback.ilearning;

/**
 * @author James MacGlashan.
 */
public interface FeedbackDiscountFunction {

	double discountForFeedback(double feedback);

	public static class ConstantFeedbackDiscountFunction implements FeedbackDiscountFunction {

		public double dicount;

		public ConstantFeedbackDiscountFunction(double dicount) {
			this.dicount = dicount;
		}

		@Override
		public double discountForFeedback(double feedback) {
			return this.dicount;
		}
	}
}
