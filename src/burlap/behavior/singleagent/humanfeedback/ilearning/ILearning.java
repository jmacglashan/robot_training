package burlap.behavior.singleagent.humanfeedback.ilearning;

import burlap.behavior.policy.BoltzmannQPolicy;
import burlap.behavior.policy.Policy;
import burlap.behavior.singleagent.EpisodeAnalysis;
import burlap.behavior.singleagent.MDPSolver;
import burlap.behavior.singleagent.humanfeedback.FeedbackReceiver;
import burlap.behavior.singleagent.learning.LearningAgent;
import burlap.behavior.singleagent.learning.tdmethods.QLearningStateNode;
import burlap.behavior.valuefunction.QFunction;
import burlap.behavior.valuefunction.QValue;
import burlap.behavior.valuefunction.ValueFunctionInitialization;
import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.AbstractObjectParameterizedGroundedAction;
import burlap.oomdp.core.Domain;
import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.GroundedAction;
import burlap.oomdp.singleagent.environment.Environment;
import burlap.oomdp.singleagent.environment.EnvironmentOutcome;
import burlap.oomdp.statehashing.HashableState;
import burlap.oomdp.statehashing.HashableStateFactory;

import javax.management.RuntimeErrorException;
import java.util.*;

/**
 * @author James MacGlashan.
 */
public class ILearning extends MDPSolver implements LearningAgent, QFunction, FeedbackReceiver{

	protected Map<HashableState, QLearningStateNode>		incomeMap = new HashMap<HashableState, QLearningStateNode>();

	protected ValueFunctionInitialization					incomeInitialization = new ValueFunctionInitialization.ConstantValueFunctionInitialization(0.);

	protected Policy										learningPolicy;

	protected FeedbackAndDiscount							lastFeedback = new FeedbackAndDiscount();

	protected FeedbackDiscountFunction discountFunction;

	protected boolean 										useEnvironmentFeedback = true;

	public ILearning(Domain domain, double discount, HashableStateFactory hashingFactory, double boltzTemp){
		this.solverInit(domain, null, null, discount, hashingFactory);
		this.learningPolicy = new BoltzmannQPolicy(this, boltzTemp);
		this.discountFunction = new FeedbackDiscountFunction.ConstantFeedbackDiscountFunction(discount);
	}

	public ILearning(Domain domain, double discount, HashableStateFactory hashingFactory, double boltzTemp, boolean useEnvironmentFeedback){
		this.solverInit(domain, null, null, discount, hashingFactory);
		this.learningPolicy = new BoltzmannQPolicy(this, boltzTemp);
		this.useEnvironmentFeedback = useEnvironmentFeedback;
		this.discountFunction = new FeedbackDiscountFunction.ConstantFeedbackDiscountFunction(discount);
	}

	public boolean isUseEnvironmentFeedback() {
		return useEnvironmentFeedback;
	}

	public void setUseEnvironmentFeedback(boolean useEnvironmentFeedback) {
		this.useEnvironmentFeedback = useEnvironmentFeedback;
	}

	public Policy getLearningPolicy() {
		return learningPolicy;
	}

	public void setLearningPolicy(Policy learningPolicy) {
		this.learningPolicy = learningPolicy;
	}

	public FeedbackDiscountFunction getDiscountFunction() {
		return discountFunction;
	}

	public void setDiscountFunction(FeedbackDiscountFunction discountFunction) {
		this.discountFunction = discountFunction;
	}

	@Override
	public EpisodeAnalysis runLearningEpisode(Environment env) {
		return this.runLearningEpisode(env, -1);
	}

	@Override
	public EpisodeAnalysis runLearningEpisode(Environment env, int maxSteps) {


		EpisodeAnalysis ea = new EpisodeAnalysis(env.getCurrentObservation());
		int steps = 0;
		while(!env.isInTerminalState() && (steps < maxSteps || maxSteps == -1)){

			GroundedAction ga = (GroundedAction)this.learningPolicy.getAction(env.getCurrentObservation());
			EnvironmentOutcome eo = ga.executeIn(env);
			ea.recordTransitionTo(ga, eo.op, eo.r);
			FeedbackAndDiscount f = this.getAndResetFeedback(eo);
			if(f.feedback != 0.){
				this.updateIncome(ea, f);
			}

			steps++;

		}

		return ea;
	}

	@Override
	public void resetSolver() {
		this.incomeMap.clear();
	}

	@Override
	public List<QValue> getQs(State s) {
		QLearningStateNode node = this.getStateNode(this.stateHash(s));
		return node.qEntry;
	}

	@Override
	public QValue getQ(State s, AbstractGroundedAction a) {
		return this.getQ(this.stateHash(s), (GroundedAction)a);
	}

	@Override
	public double value(State s) {
		return QFunctionHelper.getOptimalValue(this, s);
	}

	@Override
	synchronized public void receiveHumanFeedback(double f) {
		this.lastFeedback.feedback += f;
		double discount = this.discountFunction.discountForFeedback(f);
		if(discount > this.lastFeedback.discount){
			this.lastFeedback.discount = discount;
		}

	}

	protected synchronized FeedbackAndDiscount getAndResetFeedback(EnvironmentOutcome eo){

		if(this.useEnvironmentFeedback){
			return new FeedbackAndDiscount(eo.r, this.gamma);
		}

		FeedbackAndDiscount toRet = new FeedbackAndDiscount(this.lastFeedback);
		this.lastFeedback.reset();

		return toRet;
	}

	protected void updateIncome(EpisodeAnalysis ea, FeedbackAndDiscount feedback){

		Set<HashableState> updated = new HashSet<HashableState>(ea.maxTimeStep());
		for(int t = ea.maxTimeStep()-1; t >= 0; t--){

			HashableState s = this.stateHash(ea.getState(t));
			if(!updated.contains(s)){
				GroundedAction ga = ea.getAction(t);
				QValue q = this.getQ(s, ga);
				q.q += feedback.feedback;

				updated.add(s);
			}

			feedback.decrement();
		}

	}

	/**
	 * Returns the Q-value for a given hashed state and action.
	 * @param s the hashed state
	 * @param a the action
	 * @return the Q-value for a given hashed state and action; null is returned if there is not Q-value currently stored.
	 */
	protected QValue getQ(HashableState s, GroundedAction a) {
		QLearningStateNode node = this.getStateNode(s);

		a = (GroundedAction) AbstractObjectParameterizedGroundedAction.Helper.translateParameters(a, s.s, node.s.s);

		for(QValue qv : node.qEntry) {
			if(qv.a.equals(a)) {
				return qv;
			}
		}

		return null; //no action for this state indexed
	}


	/**
	 * Returns the {@link QLearningStateNode} object stored for the given hashed state. If no {@link QLearningStateNode} object.
	 * is stored, then it is created and has its Q-value initialize using this objects {@link burlap.behavior.valuefunction.ValueFunctionInitialization} data member.
	 * @param s the hashed state for which to get the {@link QLearningStateNode} object
	 * @return the {@link QLearningStateNode} object stored for the given hashed state. If no {@link QLearningStateNode} object.
	 */
	protected QLearningStateNode getStateNode(HashableState s){

		QLearningStateNode node = incomeMap.get(s);

		if(node == null){
			node = new QLearningStateNode(s);
			List<GroundedAction> gas = this.getAllGroundedActions(s.s);
			if(gas.size() == 0){
				gas = this.getAllGroundedActions(s.s);
				throw new RuntimeErrorException(new Error("No possible actions in this state, cannot continue Q-learning"));
			}
			for(GroundedAction ga : gas){
				node.addQValue(ga, incomeInitialization.qValue(s.s, ga));
			}

			incomeMap.put(s, node);
		}

		return node;

	}


}
