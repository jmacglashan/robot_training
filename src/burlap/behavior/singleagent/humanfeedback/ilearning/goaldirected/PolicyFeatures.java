package burlap.behavior.singleagent.humanfeedback.ilearning.goaldirected;

import burlap.behavior.policy.Policy;
import burlap.behavior.singleagent.vfa.ActionFeaturesQuery;
import burlap.behavior.singleagent.vfa.FeatureDatabase;
import burlap.behavior.singleagent.vfa.StateFeature;
import burlap.oomdp.auxiliary.stateconditiontest.StateConditionTest;
import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.GroundedAction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author James MacGlashan.
 */
public class PolicyFeatures implements FeatureDatabase{

	protected List<Policy> policies;

	public PolicyFeatures(){
		policies = new ArrayList<Policy>();
	}

	public PolicyFeatures(List<Policy> policies){
		this.policies = new ArrayList<Policy>(policies);
	}

	public PolicyFeatures(Policy...policies){
		this.policies = Arrays.asList(policies);
	}

	@Override
	public List<StateFeature> getStateFeatures(State s) {
		throw new RuntimeException("Policy features is undefined for states; only use state-action features.");
	}

	@Override
	public List<ActionFeaturesQuery> getActionFeaturesSets(State s, List<GroundedAction> actions) {

		List<ActionFeaturesQuery> afqs = new ArrayList<ActionFeaturesQuery>(actions.size());

		for(GroundedAction ga : actions){
			List<StateFeature> features = new ArrayList<StateFeature>(this.policies.size());
			int id = 0;
			for(Policy p : this.policies){
				double val = policyVal(s, ga, p);
				if(val != 0.){
					StateFeature sf = new StateFeature(id, val);
					features.add(sf);
				}
				id++;
			}

			ActionFeaturesQuery afq = new ActionFeaturesQuery(ga, features);
			afqs.add(afq);
		}

		return afqs;
	}

	@Override
	public void freezeDatabaseState(boolean toggle) {
		//nothing to do
	}

	@Override
	public int numberOfFeatures() {
		return policies.size();
	}

	protected double policyVal(State s, GroundedAction ga, Policy p){

		if(!p.isDefinedFor(s)){
			return 0;
		}

		List<Policy.ActionProb> aps = p.getActionDistributionForState(s);
		for(Policy.ActionProb ap : aps){
			if(ap.pSelection > 0 && ap.ga.equals(ga)){
				return 1.;
			}
		}

		return -1.;
	}


	@Override
	public FeatureDatabase copy() {
		return null;
	}

	public static class ConditionedPolicy extends Policy{

		Policy delegate;
		StateConditionTest initialConditions;

		public ConditionedPolicy(Policy delegate, StateConditionTest initialConditions) {
			this.delegate = delegate;
			this.initialConditions = initialConditions;
		}

		@Override
		public AbstractGroundedAction getAction(State s) {
			return delegate.getAction(s);
		}

		@Override
		public List<ActionProb> getActionDistributionForState(State s) {
			return delegate.getActionDistributionForState(s);
		}

		@Override
		public boolean isStochastic() {
			return delegate.isStochastic();
		}

		@Override
		public boolean isDefinedFor(State s) {
			return initialConditions.satisfies(s) && delegate.isDefinedFor(s);
		}
	}
}
