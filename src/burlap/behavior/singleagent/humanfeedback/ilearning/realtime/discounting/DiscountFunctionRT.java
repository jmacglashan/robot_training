package burlap.behavior.singleagent.humanfeedback.ilearning.realtime.discounting;

import burlap.behavior.singleagent.humanfeedback.ActionEvent;
import burlap.behavior.singleagent.humanfeedback.FeedbackEvent;

import java.util.List;

/**
 * @author James MacGlashan.
 */
public interface DiscountFunctionRT {
	void setFeedbackEvents(List<FeedbackEvent> events);
	double credit(ActionEvent ae, int stepsBack);
	int intMaxTime();
}
