package burlap.behavior.singleagent.humanfeedback.ilearning.realtime.discounting;

import burlap.behavior.singleagent.humanfeedback.ActionEvent;
import burlap.behavior.singleagent.humanfeedback.FeedbackEvent;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author James MacGlashan.
 */
public class GeometricStep implements DiscountFunctionRT {

	protected double defaultGamma;
	protected Map<Double, Double> gammaMap = new HashMap<Double, Double>();
	protected int stepDelay = 0;

	protected double curSignal;
	protected double curDiscount;

	protected double minDiscountForMemory = 1e-15;


	public GeometricStep(double defaultGamma) {
		this.defaultGamma = defaultGamma;
	}

	public GeometricStep(int stepDelay, double defaultGamma) {
		this.stepDelay = stepDelay;
		this.defaultGamma = defaultGamma;
	}

	public GeometricStep(int stepDelay, double defaultGamma, double minDiscountForMemory) {
		this.stepDelay = stepDelay;
		this.defaultGamma = defaultGamma;
		this.minDiscountForMemory = minDiscountForMemory;
	}

	public void setDiscountFactor(double feedbackMag, double discountFactor){
		this.gammaMap.put(feedbackMag, discountFactor);
	}

	public double getMinDiscountForMemory() {
		return minDiscountForMemory;
	}

	public void setMinDiscountForMemory(double minDiscountForMemory) {
		this.minDiscountForMemory = minDiscountForMemory;
	}

	@Override
	public void setFeedbackEvents(List<FeedbackEvent> events) {
		double mag = Double.NEGATIVE_INFINITY;
		this.curSignal = 0.;
		for(FeedbackEvent e : events){
			mag = Math.max(mag, e.r);
			this.curSignal += e.r;
		}
		this.curDiscount = this.getDisocutFactor(mag);
		if(this.curDiscount != this.defaultGamma){
			System.out.println("Special discount: " + this.curDiscount);
		}

	}

	@Override
	public double credit(ActionEvent ae, int stepsBack) {

		if(stepsBack < this.stepDelay){
			return 0.;
		}

		stepsBack -= stepDelay;
		double frac = Math.pow(this.curDiscount, stepsBack);
		double credit = frac*this.curSignal;

		return credit;
	}

	@Override
	public int intMaxTime() {

		int steps = (int)Math.round(Math.log(this.minDiscountForMemory) / Math.log(this.curDiscount));
		steps += this.stepDelay;
		return steps;
	}

	public double getDisocutFactor(double f){
		Double mapped = this.gammaMap.get(f);
		return mapped != null ? mapped : this.defaultGamma;
	}


}
