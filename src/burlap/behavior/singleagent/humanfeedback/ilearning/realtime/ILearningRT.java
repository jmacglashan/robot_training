package burlap.behavior.singleagent.humanfeedback.ilearning.realtime;

import burlap.behavior.policy.BoltzmannQPolicy;
import burlap.behavior.policy.Policy;
import burlap.behavior.singleagent.EpisodeAnalysis;
import burlap.behavior.singleagent.MDPSolver;
import burlap.behavior.singleagent.humanfeedback.ActionEvent;
import burlap.behavior.singleagent.humanfeedback.FeedbackEvent;
import burlap.behavior.singleagent.humanfeedback.FeedbackReceiver;
import burlap.behavior.singleagent.humanfeedback.ilearning.realtime.discounting.DiscountFunctionRT;
import burlap.behavior.singleagent.learning.LearningAgent;
import burlap.behavior.singleagent.vfa.DifferentiableStateActionValue;
import burlap.behavior.singleagent.vfa.FunctionGradient;
import burlap.behavior.valuefunction.QFunction;
import burlap.behavior.valuefunction.QValue;
import burlap.behavior.valuefunction.ValueFunctionInitialization;
import burlap.datastructures.HashedAggregator;
import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.Domain;
import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.Action;
import burlap.oomdp.singleagent.GroundedAction;
import burlap.oomdp.singleagent.environment.Environment;
import burlap.oomdp.singleagent.environment.EnvironmentOutcome;

import java.util.*;

/**
 * @author James MacGlashan.
 */
public class ILearningRT extends MDPSolver implements LearningAgent, QFunction, FeedbackReceiver {

	protected DifferentiableStateActionValue vfa;

	protected ValueFunctionInitialization incomeInitialization = new ValueFunctionInitialization.ConstantValueFunctionInitialization(0.);

	protected Policy learningPolicy;

	protected DiscountFunctionRT discount;

	protected boolean										useReplacingTraces = true;
	protected boolean										zeroOutOtherActions = true;

	protected double										learningRate = 1.;

	protected LinkedList<FeedbackEvent>						feedbacks = new LinkedList<FeedbackEvent>();

	protected double beginTime;


	public ILearningRT(Domain domain, DiscountFunctionRT discount, DifferentiableStateActionValue vfa, double boltzTemp) {
		this.solverInit(domain, null, null, 0., null);
		this.vfa = vfa;
		this.learningPolicy = new BoltzmannQPolicy(this, boltzTemp);
		this.discount = discount;
	}

	public Policy getLearningPolicy() {
		return learningPolicy;
	}

	public void setLearningPolicy(Policy learningPolicy) {
		this.learningPolicy = learningPolicy;
	}

	public boolean isUseReplacingTraces() {
		return useReplacingTraces;
	}

	public void setUseReplacingTraces(boolean useReplacingTraces) {
		this.useReplacingTraces = useReplacingTraces;
	}


	public double getLearningRate() {
		return learningRate;
	}

	public void setLearningRate(double learningRate) {
		this.learningRate = learningRate;
	}


	@Override
	public void receiveHumanFeedback(double f) {
		FeedbackEvent event = new FeedbackEvent(f, this.beginTime);
		synchronized(this.feedbacks){
			this.feedbacks.add(event);
		}
	}

	@Override
	public EpisodeAnalysis runLearningEpisode(Environment env) {
		return this.runLearningEpisode(env, -1);
	}

	@Override
	public EpisodeAnalysis runLearningEpisode(Environment env, int maxSteps) {

		synchronized(this.feedbacks){
			this.feedbacks.clear();
		}


		EpisodeAnalysis ea = new EpisodeAnalysis(env.getCurrentObservation());
		List<ActionEvent> aes = new ArrayList<ActionEvent>();

		beginTime = System.currentTimeMillis() / 1000.;

		int steps = 0;
		while(!env.isInTerminalState() && (steps < maxSteps || maxSteps == -1)){


			GroundedAction ga = (GroundedAction)this.learningPolicy.getAction(env.getCurrentObservation());
			aes.add(new ActionEvent(env.getCurrentObservation(), ga, this.beginTime));
			EnvironmentOutcome eo = ga.executeIn(env);
			double h = 0.;
			if(this.feedbacks.size() > 0){
				h = this.feedbacks.get(this.feedbacks.size()-1).r;
			}
			ea.recordTransitionTo(ga, eo.op, h);

			if(this.feedbacks.size() > 0){
				synchronized(this.feedbacks){
					this.updateIncome(ea, aes);
					this.feedbacks.clear();
				}
			}


			steps++;

		}


		return ea;
	}

	@Override
	public void resetSolver() {
		this.vfa.resetParameters();
	}

	@Override
	public List<QValue> getQs(State s) {

		List<GroundedAction> actions = this.getAllGroundedActions(s);
		List<QValue> qs = new ArrayList<QValue>(actions.size());
		for(GroundedAction ga : actions){
			qs.add(new QValue(s, ga, this.vfa.evaluate(s, ga)));
		}

		return qs;
	}

	@Override
	public QValue getQ(State s, AbstractGroundedAction a) {
		return new QValue(s, a, this.vfa.evaluate(s, a));
	}

	@Override
	public double value(State s) {
		return QFunctionHelper.getOptimalValue(this, s);
	}


	protected void updateIncome(EpisodeAnalysis ea, List<ActionEvent> actions){

		HashedAggregator<Integer> weightChanges = new HashedAggregator<Integer>();
		Set<Integer> markedFeatures = new HashSet<Integer>();

		this.discount.setFeedbackEvents(this.feedbacks);
		int maxStepsBack = this.discount.intMaxTime();
		int maxInd = actions.size()-1;
		int minIndex = Math.max(0, maxInd - maxStepsBack);
		for(int t = maxInd; t >= minIndex; t--){
			int stepsBack = maxInd - t;
			double f = this.discount.credit(actions.get(t), stepsBack);

			if(f == 0.){
				continue;
			}

			double pred = this.vfa.evaluate(ea.getState(t), ea.getAction(t));
			FunctionGradient gradient = this.vfa.gradient(ea.getState(t), ea.getAction(t));
			for(FunctionGradient.PartialDerivative pd : gradient.getNonZeroPartialDerivatives()){
				if(!this.useReplacingTraces || !markedFeatures.contains(pd.parameterId)){
					double delta = pd.value*f;
					weightChanges.add(pd.parameterId, delta);
					markedFeatures.add(pd.parameterId);

					if(this.zeroOutOtherActions){
						List<GroundedAction> allActions = Action.getAllApplicableGroundedActionsFromActionList(this.actions, ea.getState(t));
						for(GroundedAction oga : allActions){
							if(!oga.equals(ea.getAction(t))){
								FunctionGradient ofg = this.vfa.gradient(ea.getState(t), oga);
								for(FunctionGradient.PartialDerivative opd : ofg.getNonZeroPartialDerivatives()){
									markedFeatures.add(opd.parameterId);
								}
							}
						}
					}
				}
			}

		}

		for(Map.Entry<Integer, Double> e : weightChanges.entrySet()){
			double nw = this.vfa.getParameter(e.getKey()) + (this.learningRate * e.getValue());
			this.vfa.setParameter(e.getKey(), nw);
		}


	}


	public DifferentiableStateActionValue getVfa() {
		return vfa;
	}

	public void setVfa(DifferentiableStateActionValue vfa) {
		this.vfa = vfa;
	}

	public ValueFunctionInitialization getIncomeInitialization() {
		return incomeInitialization;
	}

	public void setIncomeInitialization(ValueFunctionInitialization incomeInitialization) {
		this.incomeInitialization = incomeInitialization;
	}

}
