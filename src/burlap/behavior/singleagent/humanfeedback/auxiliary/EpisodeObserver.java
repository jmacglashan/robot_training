package burlap.behavior.singleagent.humanfeedback.auxiliary;

import burlap.behavior.singleagent.EpisodeAnalysis;
import burlap.behavior.singleagent.learning.LearningAgent;

/**
 * @author James MacGlashan.
 */
public interface EpisodeObserver {
	void learningEpisodeComplete(LearningAgent agent, EpisodeAnalysis episode);
}
