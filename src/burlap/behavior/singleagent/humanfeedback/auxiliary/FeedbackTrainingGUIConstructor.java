package burlap.behavior.singleagent.humanfeedback.auxiliary;

import burlap.behavior.singleagent.EpisodeAnalysis;
import burlap.behavior.singleagent.MDPSolverInterface;
import burlap.behavior.singleagent.humanfeedback.FeedbackReceiver;
import burlap.behavior.singleagent.humanfeedback.HumanFeedbackEnvironment;
import burlap.behavior.singleagent.learning.LearningAgent;
import burlap.debugtools.DPrint;
import burlap.oomdp.core.Domain;
import burlap.oomdp.singleagent.environment.Environment;
import burlap.oomdp.singleagent.explorer.VisualExplorer;
import burlap.oomdp.visualizer.Visualizer;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashMap;
import java.util.Map;

/**
 * @author James MacGlashan.
 */
public class FeedbackTrainingGUIConstructor implements KeyListener{

	protected VisualExplorer exp;
	protected FeedbackReceiver receiver;
	protected HumanFeedbackEnvironment hfenv;
	protected LearningAgent agent;
	protected Map<Integer, Double> feedbackEvents = new HashMap<Integer, Double>();
	protected int terminateKeyCode = KeyEvent.getExtendedKeyCodeForChar(' ');
	protected int beginLearningKeyCode;
	protected int resetLearnerKeyCode;
	protected int expPollDelay;
	protected Thread learningThread = null;
	protected boolean sendResetOnDoubleTerminate = false;
	protected EpisodeObserver episodeObserver = null;

	protected int debugCode = 532648;

	public FeedbackTrainingGUIConstructor(Domain domain, Visualizer v, Environment env, int expPollDelay){
		this.exp = new VisualExplorer(domain, env, v);
		this.hfenv = new HumanFeedbackEnvironment(env);

		this.expPollDelay = expPollDelay;
		v.addKeyListener(this);
	}

	public int getDebugCode() {
		return debugCode;
	}

	public void setDebugCode(int debugCode) {
		this.debugCode = debugCode;
	}

	public void toggleDebugPrinting(boolean debugPrint){
		DPrint.toggleCode(this.debugCode, debugPrint);
	}

	public VisualExplorer getExp() {
		return exp;
	}

	public FeedbackReceiver getReceiver() {
		return receiver;
	}

	public void setReceiver(FeedbackReceiver receiver) {
		this.receiver = receiver;
	}

	public LearningAgent getAgent() {
		return agent;
	}

	public void setAgent(LearningAgent agent) {
		this.agent = agent;
	}

	public int getTerminateKeyCode() {
		return terminateKeyCode;
	}

	public void setTerminateKey(char terminateKey) {
		this.terminateKeyCode = KeyEvent.getExtendedKeyCodeForChar(terminateKey);
	}

	public void setTerminateKeyCode(int terminateKeyCode){
		this.terminateKeyCode = terminateKeyCode;
	}

	public boolean isSendResetOnDoubleTerminate() {
		return sendResetOnDoubleTerminate;
	}

	public void setSendResetOnDoubleTerminate(boolean sendResetOnDoubleTerminate) {
		this.sendResetOnDoubleTerminate = sendResetOnDoubleTerminate;
	}

	public void setReceiverToEnv(){
		this.receiver = this.hfenv;
	}

	public HumanFeedbackEnvironment getHfenv() {
		return hfenv;
	}



	public void setDelay(long delay, boolean delayAfter){
		this.hfenv.setDelay(delay);
		this.hfenv.setDelayAfter(delayAfter);
	}

	public int getBeginLearningKey() {
		return beginLearningKeyCode;
	}

	public void setBeginLearningKey(char beginLearningKey) {
		this.beginLearningKeyCode = KeyEvent.getExtendedKeyCodeForChar(beginLearningKey);
	}

	public int getResetLearnerKeyCode(){
		return this.resetLearnerKeyCode;
	}

	public void setResetLearnerKeyCode(int resetLearnerKeyCode){
		this.resetLearnerKeyCode = resetLearnerKeyCode;
	}

	public void setResetLearnerKey(char resetLearnerKey) {
		this.resetLearnerKeyCode = KeyEvent.getExtendedKeyCodeForChar(resetLearnerKey);
	}

	public void setBeginLearningKeyCode(int beginLearningKeyCode){
		this.beginLearningKeyCode = beginLearningKeyCode;
	}

	public void addFeedbackKey(char key, double feedback){
		this.feedbackEvents.put(KeyEvent.getExtendedKeyCodeForChar(key), feedback);
	}

	public void addFeedbackKeyCode(int keyCode, double feedback){
		this.feedbackEvents.put(keyCode, feedback);
	}

	public int findKeyCodeForFeedback(double v){
		for(Map.Entry<Integer, Double> e : this.feedbackEvents.entrySet()){
			if(v == e.getValue().doubleValue()){
				return e.getKey();
			}
		}

		return -1;
	}

	public EpisodeObserver getEpisodeObserver() {
		return episodeObserver;
	}

	public void setEpisodeObserver(EpisodeObserver episodeObserver) {
		this.episodeObserver = episodeObserver;
	}

	public void initGUI(){
		this.exp.initGUI();

		if(this.expPollDelay >= 0) {
			this.exp.startLiveStatePolling(this.expPollDelay);
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();
		if(key == this.terminateKeyCode){
			if(this.learningThread != null) {
				DPrint.cl(this.debugCode, "terminated");
				this.hfenv.giveTerminalSignal();
				try {
					this.learningThread.join();
				} catch(InterruptedException e1) {
					e1.printStackTrace();
				}
				this.hfenv.setTerminalSignal(false);
				this.learningThread = null;
			}
			else if(this.sendResetOnDoubleTerminate){
				this.hfenv.resetEnvironment();
			}
		}
		else if(key == this.beginLearningKeyCode){
			if(this.learningThread != null){
				DPrint.cl(this.debugCode, "Cannot start learning because it is already running");
				return;
			}
			DPrint.cl(this.debugCode, "started learning");
			this.learningThread = new Thread(new Runnable() {
				@Override
				public void run() {
					EpisodeAnalysis ea = agent.runLearningEpisode(hfenv);
					DPrint.cl(debugCode, "Steps: " + ea.maxTimeStep());
					if(episodeObserver != null){
						episodeObserver.learningEpisodeComplete(agent, ea);
					}
				}
			});
			this.learningThread.start();

		}
		else if(key == this.resetLearnerKeyCode){
			if(this.agent instanceof MDPSolverInterface){
				DPrint.cl(this.debugCode, "Resetting learner");
				((MDPSolverInterface)this.agent).resetSolver();
			}
			else{
				DPrint.cl(this.debugCode, "Cannot reset learner results because learner is not an implementation of MDPSolverInterface");
			}
		}
		else{
			if(this.feedbackEvents.containsKey(key)){
				double v = this.feedbackEvents.get(key);
				DPrint.cl(this.debugCode, "feedback: " + v);
				this.receiver.receiveHumanFeedback(v);
			}
		}
	}
}
