package burlap.behavior.singleagent.humanfeedback;

import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.GroundedAction;

/**
 * @author James MacGlashan.
 */
public class ActionEvent {

	public State s;
	public GroundedAction ga;
	public double t_s;
	public double t_t;

	public ActionEvent(State s, GroundedAction ga, double beginTime) {
		this.s = s;
		this.ga = ga;
		this.t_s = (System.currentTimeMillis() / 1000.) - beginTime;
	}
}
