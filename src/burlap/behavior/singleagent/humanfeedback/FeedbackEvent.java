package burlap.behavior.singleagent.humanfeedback;

/**
 * @author James MacGlashan.
 */
public class FeedbackEvent {

	public double r;
	public double timeStamp;

	public FeedbackEvent(double r, double beginTime) {
		this.r = r;
		this.timeStamp = (System.currentTimeMillis() / 1000.) - beginTime;
	}
}
