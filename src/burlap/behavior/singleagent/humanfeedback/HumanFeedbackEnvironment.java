package burlap.behavior.singleagent.humanfeedback;

import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.GroundedAction;
import burlap.oomdp.singleagent.environment.Environment;
import burlap.oomdp.singleagent.environment.EnvironmentObserver;
import burlap.oomdp.singleagent.environment.EnvironmentOutcome;

import java.util.LinkedList;
import java.util.List;

/**
 * @author James MacGlashan.
 */
public class HumanFeedbackEnvironment implements Environment, FeedbackReceiver{

	protected Environment delegate;
	protected volatile boolean terminalSignal = false;
	protected volatile double humanFeedback = 0.;
	protected double lastHumanFeedback = 0.;

	protected long delay = 0;
	protected boolean delayAfter = true;

	/**
	 * The {@link burlap.oomdp.singleagent.environment.EnvironmentObserver} objects that will be notified of {@link burlap.oomdp.singleagent.environment.Environment}
	 * events.
	 */
	protected List<EnvironmentObserver> observers = new LinkedList<EnvironmentObserver>();

	public HumanFeedbackEnvironment(Environment delegate) {
		this.delegate = delegate;
	}

	public long getDelay() {
		return delay;
	}

	public void setDelay(long delay) {
		this.delay = delay;
	}

	public boolean isDelayAfter() {
		return delayAfter;
	}

	public void setDelayAfter(boolean delayAfter) {
		this.delayAfter = delayAfter;
	}


	/**
	 * Adds one or more {@link burlap.oomdp.singleagent.environment.EnvironmentObserver}s
	 * @param observers and {@link burlap.oomdp.singleagent.environment.EnvironmentObserver}
	 */
	public void addObservers(EnvironmentObserver...observers){
		for(EnvironmentObserver observer : observers){
			this.observers.add(observer);
		}
	}

	/**
	 * Clears all {@link burlap.oomdp.singleagent.environment.EnvironmentObserver}s from this server.
	 */
	public void clearAllObservers(){
		this.observers.clear();
	}

	/**
	 * Removes one or more {@link burlap.oomdp.singleagent.environment.EnvironmentObserver}s from this server.
	 * @param observers the {@link burlap.oomdp.singleagent.environment.EnvironmentObserver}s to remove.
	 */
	public void removeObservers(EnvironmentObserver...observers){
		for(EnvironmentObserver observer : observers){
			this.observers.remove(observer);
		}
	}


	/**
	 * Returns all {@link burlap.oomdp.singleagent.environment.EnvironmentObserver}s registered with this server.
	 * @return all {@link burlap.oomdp.singleagent.environment.EnvironmentObserver}s registered with this server.
	 */
	public List<EnvironmentObserver> getObservers(){
		return this.observers;
	}





	@Override
	public State getCurrentObservation() {
		return this.delegate.getCurrentObservation();
	}

	@Override
	public EnvironmentOutcome executeAction(GroundedAction ga) {


		for(EnvironmentObserver observer : this.observers){
			observer.observeEnvironmentActionInitiation(this.getCurrentObservation(), ga);
		}

		if(!delayAfter){
			if(this.delay > 0){
				try {
					Thread.sleep(this.delay);
				} catch(InterruptedException e) {
					e.printStackTrace();
				}
			}
		}

		EnvironmentOutcome eo = this.delegate.executeAction(ga);

		if(delayAfter){
			if(this.delay > 0){
				try {
					Thread.sleep(this.delay);
				} catch(InterruptedException e) {
					e.printStackTrace();
				}
			}
		}


		if(this.terminalSignal){
			eo.terminated = true;
		}
		this.lastHumanFeedback = this.humanFeedback;
		this.humanFeedback = 0.;
		eo.r += this.lastHumanFeedback;

		for(EnvironmentObserver observer : observers){
			observer.observeEnvironmentInteraction(eo);
		}

		return eo;
	}

	@Override
	public double getLastReward() {
		return this.delegate.getLastReward() + this.lastHumanFeedback;
	}

	@Override
	public boolean isInTerminalState() {
		return this.terminalSignal || this.delegate.isInTerminalState();
	}

	public void giveTerminalSignal(){
		this.terminalSignal = true;
	}

	public void setTerminalSignal(boolean terminalSignal){
		this.terminalSignal = terminalSignal;
	}

	public boolean getTerminalSignal(){
		return this.terminalSignal;
	}

	@Override
	public void resetEnvironment() {
		this.delegate.resetEnvironment();
		this.terminalSignal = false;
		this.humanFeedback = 0.;
		this.lastHumanFeedback = 0.;
		//System.out.println("ending with terminal signal false");
		for(EnvironmentObserver observer : observers){
			observer.observeEnvironmentReset(this);
		}
	}

	@Override
	public void receiveHumanFeedback(double f) {
		this.humanFeedback += f;
	}
}
