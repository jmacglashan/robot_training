package burlap.behavior.singleagent.vfa.tabular;

import burlap.behavior.singleagent.auxiliary.StateEnumerator;
import burlap.behavior.singleagent.vfa.ActionFeaturesQuery;
import burlap.behavior.singleagent.vfa.FeatureDatabase;
import burlap.behavior.singleagent.vfa.StateFeature;
import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.GroundedAction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author James MacGlashan.
 */
public class TabularFD implements FeatureDatabase{

	StateEnumerator 								enumerator;
	Map<Integer, StoredActions> 					actionFeatures = new HashMap<Integer, StoredActions>();
	protected int									nextActionFeatureId = 0;

	public TabularFD(StateEnumerator enumerator) {
		this.enumerator = enumerator;
	}

	@Override
	public List<StateFeature> getStateFeatures(State s) {
		List <StateFeature> result = new ArrayList<StateFeature>();
		result.add(new StateFeature(enumerator.getEnumeratedID(s), 1.0));
		return result;
	}

	@Override
	public List<ActionFeaturesQuery> getActionFeaturesSets(State s, List<GroundedAction> actions) {
		int enumerated = enumerator.getEnumeratedID(s);

		List <ActionFeaturesQuery> result = new ArrayList<ActionFeaturesQuery>(actions.size());
		for(GroundedAction ga : actions){
			ActionFeaturesQuery afq = new ActionFeaturesQuery(ga);
			result.add(afq);
		}

		StoredActions sa = actionFeatures.get(enumerated);
		if(sa == null){
			sa = new StoredActions();
			for(ActionFeaturesQuery afq : result){
				sa.add(afq.queryAction, nextActionFeatureId);
				afq.addFeature(new StateFeature(nextActionFeatureId, 1.0));
				nextActionFeatureId++;
			}
			actionFeatures.put(enumerated, sa);

		}
		else{
			for(ActionFeaturesQuery afq : result){
				StoredActionFeature saf = sa.getStoredActionFeatureFor(afq.queryAction);
				if(saf == null){
					sa.add(afq.queryAction, nextActionFeatureId);
					afq.addFeature(new StateFeature(nextActionFeatureId, 1.0));
					nextActionFeatureId++;
				}
				else{
					afq.addFeature(new StateFeature(saf.id, 1.0));
				}
			}
		}

		return result;
	}

	@Override
	public void freezeDatabaseState(boolean toggle) {
		//do nothing
	}

	@Override
	public int numberOfFeatures() {
		return this.nextActionFeatureId > 0 ? this.nextActionFeatureId : this.enumerator.numStatesEnumerated();
	}

	class StoredActions{
		List<StoredActionFeature> features = new ArrayList<StoredActionFeature>();

		public void add(GroundedAction ga, int feature){
			this.features.add(new StoredActionFeature(ga, feature));
		}

		public StoredActionFeature getStoredActionFeatureFor(GroundedAction queryAction){
			for(StoredActionFeature saf : this.features){
				if(saf.srcGA.equals(queryAction)){
					return saf;
				}
			}
			StoredActionFeature saf = new StoredActionFeature(queryAction, nextActionFeatureId);
			nextActionFeatureId++;
			this.features.add(saf);
			return saf;
		}

	}

	@Override
	public TabularFD copy() {
		TabularFD tfd = new TabularFD(this.enumerator);
		tfd.actionFeatures = new HashMap<Integer, StoredActions>(this.actionFeatures);
		tfd.nextActionFeatureId = this.nextActionFeatureId;
		return tfd;
	}

	class StoredActionFeature{

		public GroundedAction			srcGA;
		public int						id;

		public StoredActionFeature(GroundedAction ga, int id){
			this.srcGA = ga;
			this.id = id;
		}

	}
}
