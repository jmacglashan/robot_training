package burlap.behavior.singleagent.vfa.tabular;

import burlap.behavior.singleagent.vfa.ActionFeaturesQuery;
import burlap.behavior.singleagent.vfa.FeatureDatabase;
import burlap.behavior.singleagent.vfa.StateFeature;
import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.GroundedAction;

import java.util.*;

/**
 * @author James MacGlashan.
 */
public class FDCombiner implements FeatureDatabase {


	protected List<FeatureDatabase>		featureDatabases;
	protected Map<IntPair, Integer>		featureIndex = new HashMap<IntPair, Integer>();
	protected int						nextFeatureId = 0;


	public FDCombiner(FeatureDatabase...fds){
		this.featureDatabases = Arrays.asList(fds);
	}

	@Override
	public List<StateFeature> getStateFeatures(State s) {

		List<List<StateFeature>> fdWiseFeatures = new ArrayList<List<StateFeature>>(this.featureDatabases.size());
		int num = 0;
		for(FeatureDatabase fd : this.featureDatabases){
			List<StateFeature> fs = fd.getStateFeatures(s);
			num += fs.size();
			fdWiseFeatures.add(fs);
		}

		List<StateFeature> combined = new ArrayList<StateFeature>(num);
		int index = 0;
		for(List<StateFeature> src : fdWiseFeatures){
			for(StateFeature sf : src){
				StateFeature csf = stateFeature(index, sf);
				combined.add(csf);
			}
			index++;
		}

		return combined;
	}

	@Override
	public List<ActionFeaturesQuery> getActionFeaturesSets(State s, List<GroundedAction> actions) {

		List<List<ActionFeaturesQuery>> fdWiseFeatures = new ArrayList<List<ActionFeaturesQuery>>(this.featureDatabases.size());
		int num = 0;
		for(FeatureDatabase fd : this.featureDatabases){
			List<ActionFeaturesQuery> fs = fd.getActionFeaturesSets(s, actions);
			num += fs.size();
			fdWiseFeatures.add(fs);
		}

		List<ActionFeaturesQuery> combined = new ArrayList<ActionFeaturesQuery>(actions.size());
		for(GroundedAction ga : actions){
			ActionFeaturesQuery afq = new ActionFeaturesQuery(ga);
			combined.add(afq);
		}
		int index = 0;
		for(List<ActionFeaturesQuery> src : fdWiseFeatures){
			int aind = 0;
			for(ActionFeaturesQuery afq : src){
				ActionFeaturesQuery cafq = this.actionFeatures(index, afq);
				combined.get(aind).features.addAll(cafq.features);
				//combined.add(cafq);
				aind++;
			}
			index++;
		}

		return combined;
	}

	@Override
	public void freezeDatabaseState(boolean toggle) {
		for(FeatureDatabase fd : this.featureDatabases){
			fd.freezeDatabaseState(toggle);
		}
	}

	@Override
	public int numberOfFeatures() {
		int num = 0;
		for(FeatureDatabase fd : this.featureDatabases){
			num += fd.numberOfFeatures();
		}
		return num;
	}

	public int getCombinedIDFor(int dbId, int dbFeatureId){
		Integer mapped = this.featureIndex.get(new IntPair(dbId, dbFeatureId));
		if(mapped == null){
			return -1;
		}
		return mapped;
	}


	protected StateFeature stateFeature(int fdId, StateFeature srcSF){

		IntPair ip = new IntPair(fdId, srcSF.id);
		Integer mapped = this.featureIndex.get(ip);
		if(mapped == null){
			mapped = nextFeatureId;
			this.featureIndex.put(ip, mapped);
			this.nextFeatureId++;
		}
		return new StateFeature(mapped, srcSF.value);


	}

	protected ActionFeaturesQuery actionFeatures(int fdId, ActionFeaturesQuery afq){

		List <StateFeature> csfs = new ArrayList<StateFeature>(afq.features.size());
		for(StateFeature sf : afq.features){
			StateFeature csf = this.stateFeature(fdId, sf);
			csfs.add(csf);
		}
		ActionFeaturesQuery cquery = new ActionFeaturesQuery(afq.queryAction, csfs);
		return cquery;

	}

	@Override
	public FeatureDatabase copy() {

		FDCombiner nfd = new FDCombiner((FeatureDatabase[])this.featureDatabases.toArray());
		nfd.featureIndex = new HashMap<IntPair, Integer>(this.featureIndex);
		nfd.nextFeatureId = this.nextFeatureId;


		return nfd;
	}

	protected static class IntPair{
		public int a;
		public int b;

		public IntPair(int a, int b){
			this.a = a;
			this.b = b;
		}


		@Override
		public int hashCode() {
			return 31*a + b;
		}

		@Override
		public boolean equals(Object obj) {
			if(!(obj instanceof IntPair)){
				return false;
			}
			IntPair o = (IntPair)obj;
			return this.a == o.a && this.b == o.b;
		}
	}

}
