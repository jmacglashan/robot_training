package experiments.turtlebot;

import burlap.oomdp.core.Domain;
import burlap.oomdp.core.states.State;
import burlap.ros.RosEnvironment;
import burlap.ros.actionpub.ActionPublisher;
import com.fasterxml.jackson.databind.JsonNode;
import experiments.turtlebot.datastructures.CameraScaleFeatures;
import experiments.turtlebot.states.TurtleScaleState;
import ros.MessageUnpacker;

/**
 * @author James MacGlashan.
 */
public class TurtleCameraEnv extends RosEnvironment {

	public TurtleCameraEnv(Domain domain, String rosBridgeURI, String rosStateTopic) {
		super(domain, rosBridgeURI, rosStateTopic, "turtle_env/CameraScaleFeatures", 1, 1);
	}

	public Domain getDomain(){
		return this.domain;
	}

	@Override
	public void receive(JsonNode data, String stringRep) {

		MessageUnpacker<CameraScaleFeatures> unpacker = new MessageUnpacker(CameraScaleFeatures.class);
		CameraScaleFeatures cf = unpacker.unpackRosMessage(data);
		State s = new TurtleScaleState(cf);
		this.curState = this.onStateReceive(s);

		if(!this.receivedFirstState){
			synchronized (this){
				this.receivedFirstState = true;
				this.notifyAll();
			}
		}

	}

	@Override
	public void resetEnvironment() {
		System.out.println("resetting environment");
		ActionPublisher ap = this.actionPublishers.get("stop");
		ap.publishAction(this.domain.getAction("stop").getAssociatedGroundedAction());
	}

}
