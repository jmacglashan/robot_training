package experiments.turtlebot;

import burlap.behavior.policy.GreedyQPolicy;
import burlap.behavior.singleagent.humanfeedback.auxiliary.FeedbackTrainingGUIConstructor;
import burlap.behavior.singleagent.humanfeedback.ilearning.realtime.ILearningRT;
import burlap.behavior.singleagent.humanfeedback.ilearning.realtime.discounting.GeometricStep;
import burlap.behavior.singleagent.humanfeedback.tamer.Tamer;
import burlap.behavior.singleagent.humanfeedback.tamer.creditassignment.UniformCreditAssignmentPDF;
import burlap.behavior.singleagent.humanfeedback.tamer.rewardmodel.GDHumanRewardModel;
import burlap.behavior.singleagent.learning.LearningAgent;
import burlap.behavior.singleagent.vfa.DifferentiableStateActionValue;
import burlap.behavior.singleagent.vfa.StateToFeatureVectorGenerator;
import burlap.behavior.singleagent.vfa.common.LinearFVVFA;
import burlap.behavior.valuefunction.QFunction;
import burlap.behavior.valuefunction.QValue;
import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.Domain;
import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.Action;
import burlap.oomdp.singleagent.SADomain;
import burlap.oomdp.singleagent.common.NullAction;
import burlap.oomdp.visualizer.Visualizer;
import burlap.ros.actionpub.CentralizedPeriodicActionPub;
import experiments.turtlebot.datastructures.SoundRequest;
import experiments.turtlebot.vfa.FVAndBoundConstant;
import experiments.turtlebot.vfa.TileCodedVision;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.DefaultCategoryDataset;
import ros.Publisher;
import ros.RosBridge;
import ros.msgs.geometry_msgs.Twist;
import ros.msgs.geometry_msgs.Vector3;
import ros.tools.PeriodicPublisher;
import tools.VFASerialization;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author James MacGlashan.
 */
public class TurtleTrainChannels {

	TurtleChannelsEnv env;
	FeedbackTrainingGUIConstructor guiConstructor;
	Set<Integer> rewardKeys = new HashSet<Integer>();
	Set<Integer> punishKeys = new HashSet<Integer>();


	public static String vfaCachePath = "vfaCache.yaml";



	public TurtleTrainChannels(String rosURI, String turltebotTwistTopic){

		//create a new domain with no state representation
		Domain domain = new SADomain();

		//create action specification (we don't need to define transition dynamics since we won't be doing any planning)
		new NullAction("forward", domain); //forward
		new NullAction("backward", domain); //backward
		new NullAction("rotate", domain); //clockwise rotate
		new NullAction("rotate_ccw", domain); //counter-clockwise rotate
		new NullAction("stop", domain); //stop/do nothing

		//setup ROS information
		String stateTopic = "/turtle_env/camera_features/channel_features";
		String actionTopic = turltebotTwistTopic;
		String actionMsg = "geometry_msgs/Twist";

		//define the relevant twist messages that we'll use for our actions
		Twist fTwist = new Twist(new Vector3(0.1,0,0.), new Vector3()); //forward
		Twist bTwist = new Twist(new Vector3(-0.1,0,0.), new Vector3()); //backward
		Twist rTwist = new Twist(new Vector3(), new Vector3(0,0,-0.5)); //clockwise rotate
		Twist rccwTwist = new Twist(new Vector3(), new Vector3(0,0,0.5)); //counter-clockwise rotate
		Twist sTwist = new Twist(); //stop/do nothing


		this.env = new TurtleChannelsEnv(domain, rosURI, stateTopic);

		//create periodic publisher
		PeriodicPublisher ppub = new PeriodicPublisher(actionTopic, actionMsg, env.getRosBridge());
		ppub.beginPublishing(sTwist, 200);

		int delay = 33;
		env.setActionPublisher("forward", new CentralizedPeriodicActionPub(ppub, fTwist, delay));
		env.setActionPublisher("backward", new CentralizedPeriodicActionPub(ppub, bTwist, delay));
		env.setActionPublisher("rotate", new CentralizedPeriodicActionPub(ppub, rTwist, delay));
		env.setActionPublisher("rotate_ccw", new CentralizedPeriodicActionPub(ppub, rccwTwist, delay));
		env.setActionPublisher("stop", new CentralizedPeriodicActionPub(ppub, sTwist, delay));

		final Visualizer v = CameraFeaturesVisualizer.getChannelsVisualizer(8);

		v.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {

			}

			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyChar()=='1'){
					((CameraFeaturesVisualizer.CameraChannelsPainter)v.getStateRenderLayer().getStaticPainters().get(0)).setChannelToPaint(0);
				}
				else if(e.getKeyChar() == '2'){
					((CameraFeaturesVisualizer.CameraChannelsPainter)v.getStateRenderLayer().getStaticPainters().get(0)).setChannelToPaint(1);
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {

			}
		});


		guiConstructor = new FeedbackTrainingGUIConstructor(domain, v, env, 33);
		guiConstructor.setSendResetOnDoubleTerminate(true);

	}

	public void addValueViewer(){

		final DefaultCategoryDataset dataset = new DefaultCategoryDataset();

		for(Action a : this.env.getDomain().getActions()){
			dataset.addValue(0., "Values", a.getName());
		}

		final JFreeChart chart = ChartFactory.createBarChart("State-action Values", "Action", "Value", dataset);


		final ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setPreferredSize(new Dimension(700, 700));


		final TextArea area = new TextArea(10, 70);
		area.setEditable(false);

		JFrame frame = new JFrame();
		//frame.setContentPane(chartPanel);
		frame.setLayout(new BorderLayout());
		frame.add(chartPanel, BorderLayout.CENTER);
		frame.add(area, BorderLayout.SOUTH);
		frame.pack();
		frame.setVisible(true);

		Thread valueUpdateThread = new Thread(new Runnable() {
			@Override
			public void run() {
				while(true) {
					QFunction agent = (QFunction) guiConstructor.getAgent();
					java.util.List<QValue> qs = agent.getQs(env.getCurrentObservation());
					for(QValue q : qs) {
						dataset.setValue(q.q, "Values", q.a.actionName());
					}
					chart.fireChartChanged();
					try {
						Thread.sleep(300);
					} catch(InterruptedException e) {
						e.printStackTrace();
					}

					DifferentiableStateActionValue vfa = null;
					if(agent instanceof Tamer){
						Tamer tagent = (Tamer)agent;
						vfa = ((GDHumanRewardModel)tagent.getHr()).getVfa();
					}
					else{
						ILearningRT iagent = (ILearningRT)agent;
						vfa = iagent.getVfa();
					}

					area.setText(linearVFAString(env.getCurrentObservation(), vfa));

				}
			}
		});
		valueUpdateThread.start();

	}


	public void addVFACaching(){

		this.guiConstructor.getExp().getVisualizer().addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {

			}

			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyChar() == ']'){
					DifferentiableStateActionValue vfa = null;
					LearningAgent agent = guiConstructor.getAgent();
					if(agent instanceof ILearningRT){
						vfa = ((ILearningRT)agent).getVfa();
					}
					else if(agent instanceof Tamer){
						vfa = ((GDHumanRewardModel)((Tamer)agent).getRF()).getVfa();
					}

					VFASerialization.serialize(vfa, vfaCachePath);

					System.out.println("saved");
				}
				else if(e.getKeyChar() == '['){

					DifferentiableStateActionValue vfa = null;
					LearningAgent agent = guiConstructor.getAgent();
					if(agent instanceof ILearningRT){
						vfa = ((ILearningRT)agent).getVfa();
					}
					else if(agent instanceof Tamer){
						vfa = ((GDHumanRewardModel)((Tamer)agent).getRF()).getVfa();
					}

					VFASerialization.deseriailize(vfa, vfaCachePath, env.getDomain());

					if(agent instanceof Tamer){
						((Tamer)agent).clearHRExtrapolation();
					}

					System.out.println("loaded");

				}
			}

			@Override
			public void keyReleased(KeyEvent e) {

			}
		});

	}



	public void addFeedbackKey(int keyCode, double feedbackValue){
		this.guiConstructor.addFeedbackKeyCode(keyCode, feedbackValue);
		if(feedbackValue > 0){
			this.rewardKeys.add(keyCode);
		}
		else{
			this.punishKeys.add(keyCode);
		}
	}

	public void addFeedbackKey(char keyChar, double feedbackValue){
		this.addFeedbackKey(KeyEvent.getExtendedKeyCodeForChar(keyChar), feedbackValue);
	}

	public void setTerminateKey(int keyCode){
		this.guiConstructor.setTerminateKeyCode(keyCode);
	}

	public void setTerminateKey(char keyChar){
		this.guiConstructor.setTerminateKey(keyChar);
	}

	public void setBeginLearningKey(int keyCode){
		this.guiConstructor.setBeginLearningKeyCode(keyCode);
	}

	public void setBeginLearningKey(char keyChar){
		this.guiConstructor.setBeginLearningKey(keyChar);
	}

	public void setResetLearnerKey(int keyCode){
		this.guiConstructor.setResetLearnerKeyCode(keyCode);
	}

	public void setResetLearnerKey(char key){
		this.guiConstructor.setResetLearnerKey(key);
	}

	/**
	 * Causes the turtle bot to play a sound when it receives a positive feedback and negative feedback
	 * @param pathToRewardSoundFile the absolute path, on the turtlebot computer, to the reward found file
	 * @param pathToPunishSoundFile the absolute path, on the turtlebot computer, to the punish sound file
	 */
	public void addFeedbackSound(String pathToRewardSoundFile, String pathToPunishSoundFile){
		this.guiConstructor.getExp().getVisualizer().addKeyListener(new FeedbackSoundPublisher(this.env.getRosBridge(), pathToRewardSoundFile, pathToPunishSoundFile));
	}


	public void startILearning(){

		//LinearFVVFA vfa = new LinearFVVFA(new ChannelsScaleInterpolateFeatures(), 0.1);
		//FVAndBoundConstant vfa = new FVAndBoundConstant(new CSINoConstant(), 0.1);
		//FVAndBoundConstant vfa = new FVAndBoundConstant(new CSSuppression(), 0.1);
		TileCodedVision vfa = new TileCodedVision(3);
		//VFASerialization.deseriailize(vfa, vfaCachePath, this.env.getDomain());
		GeometricStep gs = new GeometricStep(5, .95, 1e-10);
		//gs.setDiscountFactor(4., 0.999999);
		gs.setDiscountFactor(4., 0.9999);
		ILearningRT agent = new ILearningRT(this.env.getDomain(), gs, vfa, 1.);
		//agent.setLearningPolicy(new PoissonUncertaintyPolicy(new BoltzmannQPolicy(agent, 1.), 0.9, 1./5, 15));
		agent.setLearningPolicy(new GreedyQPolicy(agent));

		guiConstructor.setReceiver(agent);
		guiConstructor.setAgent(agent);

		guiConstructor.initGUI();

	}

	public void startTamer(){

		//LinearFVVFA vfa = new LinearFVVFA(new ChannelsScaleInterpolateFeatures(), 0.0);
		TileCodedVision vfa = new TileCodedVision(3);
		GDHumanRewardModel rf = new GDHumanRewardModel(vfa, 0.01);
		Tamer agent = new Tamer(this.env.getDomain(), rf, new UniformCreditAssignmentPDF(-0.8, -0.2), 0., 10.0);

		guiConstructor.setReceiver(agent);
		guiConstructor.setAgent(agent);

		guiConstructor.initGUI();

	}


	public class FeedbackSoundPublisher implements KeyListener {

		Publisher pub;
		SoundRequest rewardSound;
		SoundRequest punishSound;

		public FeedbackSoundPublisher(RosBridge ros, String pathToRewardSound, String pathToPunishSound){
			this.pub = new Publisher("/robotsound", "sound_play/SoundRequest", ros, true);
			this.rewardSound = new SoundRequest(pathToRewardSound);
			this.punishSound = new SoundRequest(pathToPunishSound);
		}

		@Override
		public void keyTyped(KeyEvent e) {

		}

		@Override
		public void keyPressed(KeyEvent e) {

		}

		@Override
		public void keyReleased(KeyEvent e) {
			if(rewardKeys.contains(e.getKeyCode())){
				this.pub.publish(rewardSound);
			}
			else if(punishKeys.contains(e.getKeyCode())){
				this.pub.publish(punishSound);
			}
		}
	}

	public String linearVFAString(State s, DifferentiableStateActionValue vfa){

		if(!(vfa instanceof LinearFVVFA) && !(vfa instanceof FVAndBoundConstant)){
			return "";
		}

		StringBuilder buf = new StringBuilder();

		Map<AbstractGroundedAction, Integer> actionOffset = new HashMap<AbstractGroundedAction, Integer>();
		StateToFeatureVectorGenerator fvGen;
		int paramInc = 0;
		double constScalar = 1.;

		if(vfa instanceof LinearFVVFA){
			actionOffset = ((LinearFVVFA)vfa).getActionOffset();
			fvGen = ((LinearFVVFA)vfa).getFvGen();
		}
		else{
			actionOffset = ((FVAndBoundConstant)vfa).getActionOffsets();
			fvGen = ((FVAndBoundConstant)vfa).getFvGen();
			paramInc += 1;
			constScalar = ((FVAndBoundConstant)vfa).getConstantScale();
		}
		//reverse the mapping
		Map<Integer, String> actionIndex = new HashMap<Integer, String>(actionOffset.size());
		for(Map.Entry<AbstractGroundedAction, Integer> off : actionOffset.entrySet()){
			actionIndex.put(off.getValue(), off.getKey().toString());
		}

		double [] fv = fvGen.generateFeatureVectorFrom(s);
		double actionParams = fv.length + paramInc;
		for(int i = 0; i < 95; i++){
			if(i % 19 == 0){
				buf.append(actionIndex.get(i/19)).append("\n");
			}
			int fvi = i % 19;
			if(fvi % 9 == 0){
				if(fvi == 0){
					buf.append("    pink\n");
				}
				else if(fvi == 9){
					buf.append("    orange\n");
				}
				else if(fvi == 18){
					buf.append("    constant\n");
				}
			}
			if(fvi % 3 == 0 && fvi != 18){
				if((fvi / 3) % 3 == 0){
					buf.append("        near\n");
				}
				else if((fvi / 3) % 3 == 1){
					buf.append("        mid\n");
				}
				else if((fvi / 3) % 3 == 2){
					buf.append("        far\n");
				}
			}
			double f;
			if(fvi != 18){
				f = fv[fvi];
			}
			else{
				f = constScalar;
			}
			buf.append("            ").append(f).append("    *    ").append(vfa.getParameter(i)).append("\n");
		}


		return buf.toString();
	}





	public static void main(String[] args) {

		//TurtleTrainChannels train = new TurtleTrainChannels("ws://chelone:9090", "/mobile_base/commands/velocity");
		//TurtleTrainChannels train = new TurtleTrainChannels("ws://138.16.161.182:9090", "/mobile_base/commands/velocity");
		TurtleTrainChannels train = new TurtleTrainChannels("ws://172.18.149.209:9090", "/mobile_base/commands/velocity");

		//~/rosRLAB.bash 172.18.149.209

		//my presenter remote settings
		train.addFeedbackKey(33, -1); //punish key code
		train.addFeedbackKey(34, 1); //reward key code
		train.setTerminateKey(66);
		train.setBeginLearningKey(27);
		train.setResetLearnerKey('x');
		train.addFeedbackKey('z', 4.0);

		//my wiimote settings
//		train.addFeedbackKey('a', 1.);
//		train.addFeedbackKey('b', -1);
//		train.addFeedbackKey(40, 4.0);
//		train.setBeginLearningKey('=');
//		train.setTerminateKey('-');
//		train.setResetLearnerKey(36);

		train.addFeedbackSound("/home/turtlebot/jm2_ws/src/turtle_env/scripts/Robot_blip.wav", "/home/turtlebot/jm2_ws/src/turtle_env/scripts/Robot_blip_2.wav");
		train.addVFACaching();

		train.startILearning();
		//train.startTamer();

		train.addValueViewer();


	}

}
