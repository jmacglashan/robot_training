package experiments.turtlebot.vfa;

import burlap.behavior.singleagent.vfa.DifferentiableStateActionValue;
import burlap.behavior.singleagent.vfa.FunctionGradient;
import burlap.behavior.singleagent.vfa.StateToFeatureVectorGenerator;
import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.states.State;

import java.util.HashMap;
import java.util.Map;

/**
 * @author James MacGlashan.
 */
public class FVAndBoundConstant implements DifferentiableStateActionValue {


	/**
	 * The state feature vector generator used for linear value function approximation.
	 */
	protected StateToFeatureVectorGenerator				fvGen;

	/**
	 * A feature index offset for each action when using Q-value function approximation.
	 */
	protected Map<AbstractGroundedAction, Integer> actionOffset = new HashMap<AbstractGroundedAction, Integer>();


	/**
	 * The function weights when performing Q-value function approximation.
	 */
	protected double[]									stateActionWeights;

	/**
	 * A default weight value for the functions weights.
	 */
	protected double									defaultWeight = 0.0;


	protected double 									constantScale = 1.;



	protected double[]								currentStateFeatures;
	protected int									currentActionOffset = -1;
	protected double								currentValue;
	protected State									lastState;
	protected FunctionGradient						currentGradient = null;


	/**
	 * Initializes.
	 * @param fvGen The state feature vector generator that produces the features used for either linear state value function approximation or state-action-value function approximation.
	 * @param defaultWeightValue The default weight value of all function weights.
	 */
	public FVAndBoundConstant(StateToFeatureVectorGenerator fvGen, double defaultWeightValue){
		this.fvGen = fvGen;
		this.defaultWeight = defaultWeightValue;
	}

	@Override
	public double evaluate(State s, AbstractGroundedAction a) {
		this.currentStateFeatures = this.fvGen.generateFeatureVectorFrom(s);
		this.currentActionOffset = this.getActionOffset(a);
		int indOff = this.currentActionOffset*(this.currentStateFeatures.length+1);
		double val = 0;
		for(int i = 0; i < this.currentStateFeatures.length; i++){
			val += this.currentStateFeatures[i] * this.stateActionWeights[i+indOff];
		}

		val += this.constantScale*Math.tanh(this.stateActionWeights[this.currentStateFeatures.length + indOff]);
		this.currentValue = val;
		this.currentGradient = null;
		this.lastState = s;
		return this.currentValue;
	}

	@Override
	public FunctionGradient gradient(State s, AbstractGroundedAction a) {

		double [] features;
		if(this.lastState == s){
			if(currentGradient != null){
				return this.currentGradient;
			}
			features = this.currentStateFeatures;
		}
		else{
			features = this.fvGen.generateFeatureVectorFrom(s);
		}




		FunctionGradient gradient = new FunctionGradient.SparseGradient(features.length+1);
		int sIndOffset = this.getActionOffset(a)*(features.length+1);
		for(int i = 0; i < features.length; i++){
			gradient.put(i+sIndOffset, features[i]);
		}
		double cparam = this.stateActionWeights[sIndOffset+features.length];
		double tanh = Math.tanh(cparam);
		double cpd = 1. - tanh*tanh;
		gradient.put(sIndOffset+features.length, cpd);

		this.currentGradient = gradient;
		this.currentStateFeatures = features;
		this.lastState = s;

		return gradient;

	}

	public StateToFeatureVectorGenerator getFvGen() {
		return fvGen;
	}

	public double getConstantScale() {
		return constantScale;
	}


	@Override
	public int numParameters() {
		return this.stateActionWeights.length;
	}

	@Override
	public double getParameter(int i) {
		return this.stateActionWeights[i];
	}

	@Override
	public void setParameter(int i, double p) {
		this.stateActionWeights[i] = p;
	}

	@Override
	public void resetParameters() {
		if(this.stateActionWeights != null){
			for(int i = 0; i < this.stateActionWeights.length; i++){
				this.stateActionWeights[i] = this.defaultWeight;
			}
		}
	}

	public int getActionOffset(AbstractGroundedAction a){
		Integer offset = this.actionOffset.get(a);
		if(offset == null){
			offset = this.actionOffset.size();
			this.actionOffset.put(a, offset);
			this.expandStateActionWeights(this.currentStateFeatures.length+1);
		}
		return offset;
	}



	public Map<AbstractGroundedAction, Integer> getActionOffsets(){
		return this.actionOffset;
	}

	public void setActionOffset(AbstractGroundedAction a, int offset){
		this.actionOffset.put(a, offset);
	}


	@Override
	public FVAndBoundConstant copy() {
		FVAndBoundConstant vfa = new FVAndBoundConstant(this.fvGen, this.defaultWeight);
		vfa.actionOffset = new HashMap<AbstractGroundedAction, Integer>(this.actionOffset);

		if(this.stateActionWeights != null) {
			vfa.stateActionWeights = new double[this.stateActionWeights.length];
			for(int i = 0; i < this.stateActionWeights.length; i++) {
				vfa.stateActionWeights[i] = this.stateActionWeights[i];
			}
		}


		return vfa;
	}

	/**
	 * Resets the state-action function weight array to a new array of the given sized and default value.
	 * @param size the dimensionality of the weights
	 * @param v the default value to which the weights will be set
	 */
	public void initializeStateActionWeightVector(int size, double v){
		this.stateActionWeights = new double[size];
		for(int i = 0; i < this.stateActionWeights.length; i++){
			this.stateActionWeights[i] = v;
		}
	}

	/**
	 * Expands the state-action function weight vector by a fixed sized and initializes their value
	 * to the default weight value set for this object.
	 * @param num the number of function weights to add to the state-action function weight vector
	 */
	protected void expandStateActionWeights(int num){
		double [] nWeights;
		int oldLen = 0;
		if(this.stateActionWeights!= null) {
			nWeights = new double[this.stateActionWeights.length + num];
			oldLen = this.stateActionWeights.length;
		}
		else{
			nWeights = new double[num];
		}
		for(int i = 0; i < oldLen; i++){
			nWeights[i] = this.stateActionWeights[i];
		}
		for(int i = oldLen; i < nWeights.length; i++){
			nWeights[i] = this.defaultWeight;
		}
		this.stateActionWeights = nWeights;
	}
}
