package experiments.turtlebot.featurevectors;

import burlap.behavior.singleagent.vfa.StateToFeatureVectorGenerator;
import burlap.oomdp.core.states.State;
import experiments.turtlebot.datastructures.CameraScaleFeatures;
import experiments.turtlebot.states.TurtleScaleState;


/**
 * @author James MacGlashan.
 */
public class TurtleScaleIndependent implements StateToFeatureVectorGenerator{

	@Override
	public double[] generateFeatureVectorFrom(State s) {

		int factor = 8;
		int fsq = factor*factor;

		CameraScaleFeatures csf = ((TurtleScaleState)s).features;
		double [] negFeatures = new double[csf.scales[0].features.length*csf.scales.length + 1];
		for(int i = 0; i < negFeatures.length; i++){
			negFeatures[i] = 0.;
		}
		negFeatures[negFeatures.length-1] = 1.;

		for(int r = 0; r < 8; r++){
			for(int c = 0; c < 8; c++){

				double nVal = CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 0, r, c, factor);
				double mVal = CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 0, r, c, factor);
				double fVal = CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 0, r, c, factor);
				if(nVal > 0){
					int ind = r*factor + c;
					negFeatures[ind] = 1.;
				}
				else if(mVal > 0){
					int ind = fsq + r*factor + c;
					negFeatures[ind] = 1.;
				}
				else if(fVal > 0){
					int ind = 2*fsq + r*factor + c;
					negFeatures[ind] = 1.;
				}


			}
		}

		return negFeatures;


	}
}
