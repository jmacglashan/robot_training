package experiments.turtlebot.featurevectors;

import burlap.behavior.singleagent.vfa.StateToFeatureVectorGenerator;
import burlap.oomdp.core.states.State;
import experiments.turtlebot.datastructures.CameraScaleFeatures;
import experiments.turtlebot.states.TurtleScaleState;

/**
 * @author James MacGlashan.
 */
public class ScaleSimplifierInterpolate implements StateToFeatureVectorGenerator {

	@Override
	public double[] generateFeatureVectorFrom(State s) {
		double [] lowResMid = new double[10];

		CameraScaleFeatures csf = ((TurtleScaleState)s).features;


		double nearVals = 0;
		for(int c = 0; c < 3; c++){
			for(int r = 0; r < 8; r++){
				if(c != 2) {
					lowResMid[0] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 0, r, c, 8) / 255.;
				}
				else{
					lowResMid[0] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 0, r, c, 8) / (255.*2);
				}
			}
		}

		for(int c = 2; c < 6; c++){
			for(int r = 0; r < 8; r++){
				if(c != 2 && c != 5) {
					lowResMid[1] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 0, r, c, 8) / 255.;
				}
				else{
					lowResMid[1] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 0, r, c, 8) / (255.*2);
				}
			}
		}

		for(int c = 5; c < 8; c++){
			for(int r = 0; r < 8; r++){
				if(c != 5) {
					lowResMid[2] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 0, r, c, 8) / 255.;
				}
				else{
					lowResMid[2] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 0, r, c, 8) / (255.*2);
				}
			}
		}
		nearVals += lowResMid[0] + lowResMid[1] + lowResMid[2];


		//now do mid
		int offset = 3;
		boolean useNear = true;
		double midVals = 0.;

		for(int c = 0; c < 3; c++){
			for(int r = 0; r < 8; r++){
				if(useNear && nearVals == 0.) {
					if(c != 2) {
						lowResMid[0 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 1, r, c, 8) / 255.;
					}
					else{
						lowResMid[0 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 1, r, c, 8) / (255.*2);
					}
				}
			}
		}

		for(int c = 2; c < 6; c++){
			for(int r = 0; r < 8; r++){
				if(useNear && nearVals == 0.) {
					if(c != 2 && c != 5) {
						lowResMid[1 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 1, r, c, 8) / 255.;
					}
					else{
						lowResMid[1 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 1, r, c, 8) / (255.*2);
					}
				}
			}
		}

		for(int c = 5; c < 8; c++){
			for(int r = 0; r < 8; r++){
				if(useNear && nearVals == 0.) {
					if(c != 5) {
						lowResMid[2 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 1, r, c, 8) / 255.;
					}
					else{
						lowResMid[2 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 1, r, c, 8) / (255.*2);
					}
				}
			}
		}

		midVals = lowResMid[offset+0] + lowResMid[offset+1] + lowResMid[offset+2];


		//now do far
		offset = 6;

		for(int c = 0; c < 3; c++){
			for(int r = 0; r < 8; r++){
				if(useNear && midVals == 0. && nearVals == 0.) {
					if(c != 2) {
						lowResMid[0 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 2, r, c, 8) / 255.;
					}
					else{
						lowResMid[0 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 2, r, c, 8) / (255.*2);
					}
				}
			}
		}

		for(int c = 2; c < 6; c++){
			for(int r = 0; r < 8; r++){
				if(useNear && midVals == 0. && nearVals == 0.) {
					if(c != 2 && c != 5) {
						lowResMid[1 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 2, r, c, 8) / 255.;
					}
					else{
						lowResMid[1 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 2, r, c, 8) / (255.*2);
					}
				}
			}
		}

		for(int c = 5; c < 8; c++){
			for(int r = 0; r < 8; r++){
				if(useNear && midVals == 0. && nearVals == 0.) {
					if(c != 5) {
						lowResMid[2 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 2, r, c, 8) / 255.;
					}
					else{
						lowResMid[2 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 2, r, c, 8) / (255.*2);
					}
				}
			}
		}

		lowResMid[lowResMid.length-1] = 1.;

		//bbb1bSystem.out.println(Arrays.toString(lowResMid));


		return lowResMid;
	}
}
