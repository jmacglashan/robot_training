package experiments.turtlebot.featurevectors;

import burlap.behavior.singleagent.vfa.StateToFeatureVectorGenerator;
import burlap.oomdp.core.states.State;
import experiments.turtlebot.datastructures.CameraScaleFeatures;
import experiments.turtlebot.states.TurtleChannelsState;

/**
 * @author James MacGlashan.
 */
public class CSINoConstant implements StateToFeatureVectorGenerator {

	@Override
	public double[] generateFeatureVectorFrom(State s) {

		TurtleChannelsState ts = (TurtleChannelsState)s;

		double [] fvec = new double[18];
		this.generateChannelFeatures(ts.channels.getChannel(0), fvec, 0);
		this.generateChannelFeatures(ts.channels.getChannel(1), fvec, 9);

		return fvec;
	}


	protected void generateChannelFeatures(CameraScaleFeatures csf, double [] lowResMid, int offset){

		double nearVals = 0;
		for(int c = 0; c < 3; c++){
			for(int r = 0; r < 8; r++){
				if(c != 2) {
					lowResMid[0+offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 0, r, c, 8) / 255.;
				}
				else{
					lowResMid[0+offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 0, r, c, 8) / (255.*2);
				}
			}
		}

		for(int c = 2; c < 6; c++){
			for(int r = 0; r < 8; r++){
				if(c != 2 && c != 5) {
					lowResMid[1+offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 0, r, c, 8) / 255.;
				}
				else{
					lowResMid[1+offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 0, r, c, 8) / (255.*2);
				}
			}
		}

		for(int c = 5; c < 8; c++){
			for(int r = 0; r < 8; r++){
				if(c != 5) {
					lowResMid[2+offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 0, r, c, 8) / 255.;
				}
				else{
					lowResMid[2+offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 0, r, c, 8) / (255.*2);
				}
			}
		}

		this.normalize(lowResMid, offset, 3);
		nearVals += lowResMid[0+offset] + lowResMid[1+offset] + lowResMid[2+offset];


		//now do mid
		offset += 3;
		boolean useNear = true;
		double midVals = 0.;

		for(int c = 0; c < 3; c++){
			for(int r = 0; r < 8; r++){
				if(useNear && nearVals == 0.) {
					if(c != 2) {
						lowResMid[0 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 1, r, c, 8) / 255.;
					}
					else{
						lowResMid[0 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 1, r, c, 8) / (255.*2);
					}
				}
			}
		}

		for(int c = 2; c < 6; c++){
			for(int r = 0; r < 8; r++){
				if(useNear && nearVals == 0.) {
					if(c != 2 && c != 5) {
						lowResMid[1 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 1, r, c, 8) / 255.;
					}
					else{
						lowResMid[1 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 1, r, c, 8) / (255.*2);
					}
				}
			}
		}

		for(int c = 5; c < 8; c++){
			for(int r = 0; r < 8; r++){
				if(useNear && nearVals == 0.) {
					if(c != 5) {
						lowResMid[2 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 1, r, c, 8) / 255.;
					}
					else{
						lowResMid[2 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 1, r, c, 8) / (255.*2);
					}
				}
			}
		}

		this.normalize(lowResMid, offset, 3);
		midVals = lowResMid[offset+0] + lowResMid[offset+1] + lowResMid[offset+2];


		//now do far
		offset += 3;

		for(int c = 0; c < 3; c++){
			for(int r = 0; r < 8; r++){
				if(useNear && midVals == 0. && nearVals == 0.) {
					if(c != 2) {
						lowResMid[0 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 2, r, c, 8) / 255.;
					}
					else{
						lowResMid[0 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 2, r, c, 8) / (255.*2);
					}
				}
			}
		}

		for(int c = 2; c < 6; c++){
			for(int r = 0; r < 8; r++){
				if(useNear && midVals == 0. && nearVals == 0.) {
					if(c != 2 && c != 5) {
						lowResMid[1 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 2, r, c, 8) / 255.;
					}
					else{
						lowResMid[1 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 2, r, c, 8) / (255.*2);
					}
				}
			}
		}

		for(int c = 5; c < 8; c++){
			for(int r = 0; r < 8; r++){
				if(useNear && midVals == 0. && nearVals == 0.) {
					if(c != 5) {
						lowResMid[2 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 2, r, c, 8) / 255.;
					}
					else{
						lowResMid[2 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 2, r, c, 8) / (255.*2);
					}
				}
			}
		}

		this.normalize(lowResMid, offset, 3);

	}

	protected void normalize(double [] fv, int s, int l){

		double sum = 0.;
		for(int i = s; i < s+l; i++){
			sum += fv[i];
		}

		if(sum < 1){
			sum = 1;
		}

		for(int i = s; i < s+l; i++){
			fv[i] /= sum;
		}

	}
}
