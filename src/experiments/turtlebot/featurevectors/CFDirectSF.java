package experiments.turtlebot.featurevectors;

import burlap.behavior.singleagent.vfa.StateToFeatureVectorGenerator;
import burlap.oomdp.core.states.State;
import experiments.turtlebot.states.TurtleCameraState;


/**
 * @author James MacGlashan.
 */
public class CFDirectSF implements StateToFeatureVectorGenerator {

	@Override
	public double[] generateFeatureVectorFrom(State s) {
		double [] srcFeatures =  ((TurtleCameraState)s).features.features;
		double [] negFeatures = srcFeatures.clone();
		for(int i = 0; i < negFeatures.length; i++){
			if(negFeatures[i] == 0.){
				negFeatures[i] = -1.;
			}
		}
		return negFeatures;
	}
}
