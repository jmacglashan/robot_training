package experiments.turtlebot.featurevectors;

import burlap.behavior.singleagent.vfa.StateToFeatureVectorGenerator;
import burlap.oomdp.core.states.State;
import experiments.turtlebot.datastructures.CameraScaleFeatures;
import experiments.turtlebot.states.TurtleScaleState;


/**
 * @author James MacGlashan.
 */
public class ScaleSimplifer implements StateToFeatureVectorGenerator {

	@Override
	public double[] generateFeatureVectorFrom(State s) {

		double [] lowResMid = new double[10];

		CameraScaleFeatures csf = ((TurtleScaleState)s).features;


		for(int c = 0; c < 2; c++){
			for(int r = 0; r < 8; r++){
				lowResMid[0] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 0, r, c, 8) / 255.;
			}
		}

		for(int c = 2; c < 6; c++){
			for(int r = 0; r < 8; r++){
				lowResMid[1] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 0, r, c, 8) /255.;
			}
		}

		for(int c = 6; c < 8; c++){
			for(int r = 0; r < 8; r++){
				lowResMid[2] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 0, r, c, 8) /255.;
			}
		}


		//now do mid
		int offset = 3;
		boolean useNear = true;

		for(int c = 0; c < 2; c++){
			for(int r = 0; r < 8; r++){
				if(useNear && lowResMid[0] == 0.) {
					lowResMid[0 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 1, r, c, 8) / 255.;
				}
			}
		}

		for(int c = 2; c < 6; c++){
			for(int r = 0; r < 8; r++){
				if(useNear && lowResMid[1] == 0.) {
					lowResMid[1 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 1, r, c, 8) / 255.;
				}
			}
		}

		for(int c = 6; c < 8; c++){
			for(int r = 0; r < 8; r++){
				if(useNear && lowResMid[2] == 0.) {
					lowResMid[2 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 1, r, c, 8) / 255.;
				}
			}
		}


		//now do far
		offset = 6;

		for(int c = 0; c < 2; c++){
			for(int r = 0; r < 8; r++){
				if(useNear && lowResMid[3] == 0.) {
					lowResMid[0 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 2, r, c, 8) / 255.;;
				}
			}
		}

		for(int c = 2; c < 6; c++){
			for(int r = 0; r < 8; r++){
				if(useNear && lowResMid[4] == 0.) {
					lowResMid[1 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 2, r, c, 8) / 255.;
				}
			}
		}

		for(int c = 6; c < 8; c++){
			for(int r = 0; r < 8; r++){
				if(useNear && lowResMid[5] == 0.) {
					lowResMid[2 + offset] += CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 2, r, c, 8) / 255.;
				}
			}
		}

		lowResMid[lowResMid.length-1] = 1.;

		//System.out.println(Arrays.toString(lowResMid));


		return lowResMid;
	}
}
