package experiments.turtlebot.featurevectors;

import burlap.behavior.singleagent.vfa.StateToFeatureVectorGenerator;
import burlap.oomdp.core.states.State;
import experiments.turtlebot.datastructures.CameraScaleFeatures;
import experiments.turtlebot.states.TurtleScaleState;


/**
 * @author James MacGlashan.
 */
public class CFScaleSF implements StateToFeatureVectorGenerator {

	@Override
	public double[] generateFeatureVectorFrom(State s) {

		CameraScaleFeatures csf = ((TurtleScaleState)s).features;
		double [] negFeatures = new double[csf.scales[0].features.length*csf.scales.length];
		int c = 0;
		for(int q = 0; q < csf.scales.length; q++){
			for(int i = 0; i < csf.scales[q].features.length; i++){
				double v = csf.scales[q].features[i];
				if(v == 0.){
					v = -1.;
				}
				negFeatures[c] = v;
				c++;
			}
		}

		return negFeatures;
	}
}
