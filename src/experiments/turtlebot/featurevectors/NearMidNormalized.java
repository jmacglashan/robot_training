package experiments.turtlebot.featurevectors;

import burlap.behavior.singleagent.vfa.StateToFeatureVectorGenerator;
import burlap.oomdp.core.states.State;
import experiments.turtlebot.datastructures.CameraScaleFeatures;
import experiments.turtlebot.states.TurtleScaleState;


/**
 * @author James MacGlashan.
 */
public class NearMidNormalized implements StateToFeatureVectorGenerator {

	@Override
	public double[] generateFeatureVectorFrom(State s) {

		double [] vec = new double[129];

		CameraScaleFeatures csf = ((TurtleScaleState)s).features;

		int i = 0;
		for(int c = 0; c < 8; c++){
			for(int r = 0; r < 8; r++){
				vec[i] = CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 0, r, c, 8) / 255.;
				i++;
			}
		}

		for(int c = 0; c < 8; c++){
			for(int r = 0; r < 8; r++){
				vec[i] = CameraScaleFeatures.CameraScaleFeaturesHelper.getFeatures(csf, 1, r, c, 8) /255.;
				i++;
			}
		}


		vec[vec.length-1] = 1.;
		return vec;
	}
}
