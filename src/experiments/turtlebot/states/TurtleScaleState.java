package experiments.turtlebot.states;

import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.State;
import experiments.turtlebot.datastructures.CameraScaleFeatures;

import java.util.*;

/**
 * @author James MacGlashan.
 */
public class TurtleScaleState implements State{

	public CameraScaleFeatures features;

	public TurtleScaleState(CameraScaleFeatures features){
		this.features = features;
	}

	@Override
	public State copy() {
		return new TurtleScaleState(this.features.copy());
	}

	@Override
	public State addObject(ObjectInstance o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public State addAllObjects(Collection<ObjectInstance> objects) {
		throw new UnsupportedOperationException();
	}

	@Override
	public State removeObject(String oname) {
		throw new UnsupportedOperationException();
	}

	@Override
	public <T> State setObjectsValue(String objectName, String attName, T value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public State removeObject(ObjectInstance o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public State removeAllObjects(Collection<ObjectInstance> objects) {
		throw new UnsupportedOperationException();
	}

	@Override
	public State renameObject(String originalName, String newName) {
		throw new UnsupportedOperationException();
	}

	@Override
	public State renameObject(ObjectInstance o, String newName) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Map<String, String> getObjectMatchingTo(State so, boolean enforceStateExactness) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int numTotalObjects() {
		return 0;
	}

	@Override
	public ObjectInstance getObject(String oname) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<ObjectInstance> getAllObjects() {
		return new ArrayList<ObjectInstance>();
	}

	@Override
	public List<ObjectInstance> getObjectsOfClass(String oclass) {
		throw new UnsupportedOperationException();
	}

	@Override
	public ObjectInstance getFirstObjectOfClass(String oclass) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Set<String> getObjectClassesPresent() {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<List<ObjectInstance>> getAllObjectsByClass() {
		throw new UnsupportedOperationException();
	}

	@Override
	public String getCompleteStateDescription() {
		return Arrays.toString(this.features.scales);
	}

	@Override
	public Map<String, List<String>> getAllUnsetAttributes() {
		return new HashMap<String, List<String>>();
	}

	@Override
	public String getCompleteStateDescriptionWithUnsetAttributesAsNull() {
		return this.getCompleteStateDescription();
	}

	@Override
	public List<List<String>> getPossibleBindingsGivenParamOrderGroups(String[] paramClasses, String[] paramOrderGroups) {
		throw new UnsupportedOperationException();
	}

}
