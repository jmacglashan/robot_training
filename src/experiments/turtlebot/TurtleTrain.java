package experiments.turtlebot;

import burlap.behavior.policy.GreedyQPolicy;
import burlap.behavior.singleagent.humanfeedback.auxiliary.FeedbackTrainingGUIConstructor;
import burlap.behavior.singleagent.humanfeedback.ilearning.realtime.ILearningRT;
import burlap.behavior.singleagent.humanfeedback.ilearning.realtime.discounting.GeometricStep;
import burlap.behavior.singleagent.humanfeedback.tamer.Tamer;
import burlap.behavior.singleagent.humanfeedback.tamer.creditassignment.UniformCreditAssignmentPDF;
import burlap.behavior.singleagent.humanfeedback.tamer.rewardmodel.GDHumanRewardModel;
import burlap.behavior.singleagent.vfa.DifferentiableStateActionValue;
import burlap.behavior.singleagent.vfa.common.LinearFVVFA;
import burlap.behavior.valuefunction.QFunction;
import burlap.behavior.valuefunction.QValue;
import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.Domain;
import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.Action;
import burlap.oomdp.singleagent.SADomain;
import burlap.oomdp.singleagent.common.NullAction;
import burlap.oomdp.visualizer.Visualizer;
import burlap.ros.actionpub.CentralizedPeriodicActionPub;
import experiments.turtlebot.datastructures.SoundRequest;
import experiments.turtlebot.featurevectors.ScaleSimplifierInterpolate;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.DefaultCategoryDataset;
import ros.Publisher;
import ros.RosBridge;
import ros.msgs.geometry_msgs.Twist;
import ros.msgs.geometry_msgs.Vector3;
import ros.tools.PeriodicPublisher;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.*;
import java.util.List;

/**
 * @author James MacGlashan.
 */
public class TurtleTrain {

	TurtleCameraEnv env;
	FeedbackTrainingGUIConstructor guiConstructor;
	Set<Integer> rewardKeys = new HashSet<Integer>();
	Set<Integer> punishKeys = new HashSet<Integer>();



	public TurtleTrain(String rosURI, String turltebotTwistTopic){

		//create a new domain with no state representation
		Domain domain = new SADomain();

		//create action specification (we don't need to define transition dynamics since we won't be doing any planning)
		new NullAction("forward", domain); //forward
		new NullAction("backward", domain); //backward
		new NullAction("rotate", domain); //clockwise rotate
		new NullAction("rotate_ccw", domain); //counter-clockwise rotate
		new NullAction("stop", domain); //stop/do nothing

		//setup ROS information
		String stateTopic = "/turtle_env/camera_features/scale_features";
		String actionTopic = turltebotTwistTopic;
		String actionMsg = "geometry_msgs/Twist";

		//define the relevant twist messages that we'll use for our actions
		Twist fTwist = new Twist(new Vector3(0.1,0,0.), new Vector3()); //forward
		Twist bTwist = new Twist(new Vector3(-0.1,0,0.), new Vector3()); //backward
		Twist rTwist = new Twist(new Vector3(), new Vector3(0,0,-0.5)); //clockwise rotate
		Twist rccwTwist = new Twist(new Vector3(), new Vector3(0,0,0.5)); //counter-clockwise rotate
		Twist sTwist = new Twist(); //stop/do nothing


		this.env = new TurtleCameraEnv(domain, rosURI, stateTopic);

		//create periodic publisher
		PeriodicPublisher ppub = new PeriodicPublisher(actionTopic, actionMsg, env.getRosBridge());
		ppub.beginPublishing(sTwist, 200);

		int delay = 33;
		env.setActionPublisher("forward", new CentralizedPeriodicActionPub(ppub, fTwist, delay));
		env.setActionPublisher("backward", new CentralizedPeriodicActionPub(ppub, bTwist, delay));
		env.setActionPublisher("rotate", new CentralizedPeriodicActionPub(ppub, rTwist, delay));
		env.setActionPublisher("rotate_ccw", new CentralizedPeriodicActionPub(ppub, rccwTwist, delay));
		env.setActionPublisher("stop", new CentralizedPeriodicActionPub(ppub, sTwist, delay));

		Visualizer v = CameraFeaturesVisualizer.getScaleVisualizer(8);

		guiConstructor = new FeedbackTrainingGUIConstructor(domain, v, env, 33);
		guiConstructor.setSendResetOnDoubleTerminate(true);

	}

	public void addValueViewer(){

		final DefaultCategoryDataset dataset = new DefaultCategoryDataset();

		for(Action a : this.env.getDomain().getActions()){
			dataset.addValue(0., "Values", a.getName());
		}

		final JFreeChart chart = ChartFactory.createBarChart("State-action Values", "Action", "Value", dataset);


		final ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setPreferredSize(new Dimension(700, 700));


		final TextArea area = new TextArea(10, 70);
		area.setEditable(false);

		JFrame frame = new JFrame();
		//frame.setContentPane(chartPanel);
		frame.setLayout(new BorderLayout());
		frame.add(chartPanel, BorderLayout.CENTER);
		frame.add(area, BorderLayout.SOUTH);
		frame.pack();
		frame.setVisible(true);

		Thread valueUpdateThread = new Thread(new Runnable() {
			@Override
			public void run() {
				while(true) {
					QFunction agent = (QFunction) guiConstructor.getAgent();
					List<QValue> qs = agent.getQs(env.getCurrentObservation());
					for(QValue q : qs) {
						dataset.setValue(q.q, "Values", q.a.actionName());
					}
					chart.fireChartChanged();
					try {
						Thread.sleep(250);
					} catch(InterruptedException e) {
						e.printStackTrace();
					}

					DifferentiableStateActionValue vfa = null;
					if(agent instanceof Tamer){
						Tamer tagent = (Tamer)agent;
						vfa = ((GDHumanRewardModel)tagent.getHr()).getVfa();
					}
					else{
						ILearningRT iagent = (ILearningRT)agent;
						vfa = iagent.getVfa();
					}

					area.setText(linearVFAString(env.getCurrentObservation(), vfa));

				}
			}
		});
		valueUpdateThread.start();

	}

	public void addFeedbackKey(int keyCode, double feedbackValue){
		this.guiConstructor.addFeedbackKeyCode(keyCode, feedbackValue);
		if(feedbackValue > 0){
			this.rewardKeys.add(keyCode);
		}
		else{
			this.punishKeys.add(keyCode);
		}
	}

	public void addFeedbackKey(char keyChar, double feedbackValue){
		this.addFeedbackKey(KeyEvent.getExtendedKeyCodeForChar(keyChar), feedbackValue);
	}

	public void setTerminateKey(int keyCode){
		this.guiConstructor.setTerminateKeyCode(keyCode);
	}

	public void setTerminateKey(char keyChar){
		this.guiConstructor.setTerminateKey(keyChar);
	}

	public void setBeginLearningKey(int keyCode){
		this.guiConstructor.setBeginLearningKeyCode(keyCode);
	}

	public void setBeginLearningKey(char keyChar){
		this.guiConstructor.setBeginLearningKey(keyChar);
	}

	public void setResetLearnerKey(int keyCode){
		this.guiConstructor.setResetLearnerKeyCode(keyCode);
	}

	public void setResetLearnerKey(char key){
		this.guiConstructor.setResetLearnerKey(key);
	}

	/**
	 * Causes the turtle bot to play a sound when it receives a positive feedback and negative feedback
	 * @param pathToRewardSoundFile the absolute path, on the turtlebot computer, to the reward found file
	 * @param pathToPunishSoundFile the absolute path, on the turtlebot computer, to the punish sound file
	 */
	public void addFeedbackSound(String pathToRewardSoundFile, String pathToPunishSoundFile){
		this.guiConstructor.getExp().getVisualizer().addKeyListener(new FeedbackSoundPublisher(this.env.getRosBridge(), pathToRewardSoundFile, pathToPunishSoundFile));
	}


	public void startILearning(){

		LinearFVVFA vfa = new LinearFVVFA(new ScaleSimplifierInterpolate(), 0.1);
		ILearningRT agent = new ILearningRT(this.env.getDomain(), new GeometricStep(6, 0.95, 1e-10), vfa, 1.);
		//agent.setLearningPolicy(new PoissonUncertaintyPolicy(new BoltzmannQPolicy(agent, 1.), 0.9, 1./5, 15));
		agent.setLearningPolicy(new GreedyQPolicy(agent));

		guiConstructor.setReceiver(agent);
		guiConstructor.setAgent(agent);

		guiConstructor.initGUI();

	}

	public void startTamer(){

		LinearFVVFA vfa = new LinearFVVFA(new ScaleSimplifierInterpolate(), 0.0);
		GDHumanRewardModel rf = new GDHumanRewardModel(vfa, 0.1);
		Tamer agent = new Tamer(this.env.getDomain(), rf, new UniformCreditAssignmentPDF(-0.8, -0.2), 0., 10.0);

		guiConstructor.setReceiver(agent);
		guiConstructor.setAgent(agent);

		guiConstructor.initGUI();

	}


	public class FeedbackSoundPublisher implements KeyListener{

		Publisher pub;
		SoundRequest rewardSound;
		SoundRequest punishSound;

		public FeedbackSoundPublisher(RosBridge ros, String pathToRewardSound, String pathToPunishSound){
			this.pub = new Publisher("/robotsound", "sound_play/SoundRequest", ros, true);
			this.rewardSound = new SoundRequest(pathToRewardSound);
			this.punishSound = new SoundRequest(pathToPunishSound);
		}

		@Override
		public void keyTyped(KeyEvent e) {

		}

		@Override
		public void keyPressed(KeyEvent e) {

		}

		@Override
		public void keyReleased(KeyEvent e) {
			if(rewardKeys.contains(e.getKeyCode())){
				this.pub.publish(rewardSound);
			}
			else if(punishKeys.contains(e.getKeyCode())){
				this.pub.publish(punishSound);
			}
		}
	}

	public String linearVFAString(State s, DifferentiableStateActionValue vfa){

		StringBuilder buf = new StringBuilder();

		double [] fullSF = new double[50];
		Map<Integer, String> actionIndex = new HashMap<Integer, String>();


		LinearFVVFA lvfa = (LinearFVVFA)vfa;
		Map<AbstractGroundedAction, Integer> actionOffsets = lvfa.getActionOffset();


		/*
		List<ActionApproximationResult> aprs = vfa.getStateActionValues(s, Action.getAllApplicableGroundedActionsFromActionList(this.env.getDomain().getActions(), s));
		for(ActionApproximationResult apr : aprs){
			for(StateFeature sf : apr.approximationResult.stateFeatures){
				fullSF[sf.id] = sf.value;
				actionIndex.put(sf.id / 10, apr.ga.toString());
			}
		}

		for(int i = 0; i < 50; i++){
			if(i % 10 == 0){
				buf.append(actionIndex.get(i/10)).append("\n");
			}
			buf.append("     ").append(fullSF[i]).append("    *    ").append(vfa.getFunctionWeight(i).weightValue()).append("\n");
		}
		*/


		return buf.toString();
	}






	public static void main(String[] args) {

		//TurtleTrain train = new TurtleTrain("ws://chelone:9090", "/mobile_base/commands/velocity");
		TurtleTrain train = new TurtleTrain("ws://138.16.161.182:9090", "/mobile_base/commands/velocity");

		//my presenter remote settings
//		train.addFeedbackKey(33, -1); //punish key code
//		train.addFeedbackKey(34, 1); //reward key code
//		train.setTerminateKey(66);
//		train.setBeginLearningKey(27);

		//my wiimote settings
		train.addFeedbackKey('a', 1.);
		train.addFeedbackKey('b', -1);
		train.setBeginLearningKey('=');
		train.setTerminateKey('-');
		train.setResetLearnerKey(36);

		train.addFeedbackSound("/home/turtlebot/jm2_ws/src/turtle_env/scripts/Robot_blip.wav", "/home/turtlebot/jm2_ws/src/turtle_env/scripts/Robot_blip_2.wav");

		train.startILearning();
		//train.startTamer();

		train.addValueViewer();


	}


}
