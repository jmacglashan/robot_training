package experiments.simulated.human;

import burlap.oomdp.core.TransitionProbability;
import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.Action;
import burlap.oomdp.singleagent.FullActionModel;
import burlap.oomdp.singleagent.GroundedAction;

import java.util.ArrayList;
import java.util.List;

/**
 * @author James MacGlashan.
 */
public abstract class AddedPreconditionAction extends Action implements FullActionModel{

	public Action delegate;

	public AddedPreconditionAction(Action delegate){
		this.name = delegate.getName();
		this.delegate = delegate;
	}

	@Override
	public abstract boolean applicableInState(State s, GroundedAction groundedAction);

	@Override
	protected State performActionHelper(State s, GroundedAction groundedAction) {
		return delegate.performAction(s, groundedAction);
	}

	@Override
	public List<TransitionProbability> getTransitions(State s, GroundedAction groundedAction) {
		return ((FullActionModel) delegate).getTransitions(s, groundedAction);
	}

	@Override
	public boolean isPrimitive() {
		return delegate.isPrimitive();
	}

	@Override
	public boolean isParameterized() {
		return delegate.isParameterized();
	}

	@Override
	public GroundedAction getAssociatedGroundedAction() {
		GroundedAction ga = delegate.getAssociatedGroundedAction();
		ga.action = this;
		return ga;
	}

	@Override
	public List<GroundedAction> getAllApplicableGroundedActions(State s) {
		List<GroundedAction> srcActions = this.delegate.getAllApplicableGroundedActions(s);
		List<GroundedAction> refChanges = new ArrayList<GroundedAction>(srcActions.size());
		for(GroundedAction ga : srcActions){
			GroundedAction refChange = ga.copy();
			refChange.action = this;
			if(this.applicableInState(s, refChange)) {
				refChanges.add(refChange);
			}
		}
		return refChanges;
	}

}
