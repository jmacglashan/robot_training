package experiments.simulated.human;

import burlap.behavior.policy.GreedyQPolicy;
import burlap.behavior.singleagent.EpisodeAnalysis;
import burlap.behavior.singleagent.auxiliary.StateEnumerator;
import burlap.behavior.singleagent.humanfeedback.auxiliary.EpisodeObserver;
import burlap.behavior.singleagent.humanfeedback.auxiliary.FeedbackTrainingGUIConstructor;
import burlap.behavior.singleagent.humanfeedback.ilearning.realtime.ILearningRT;
import burlap.behavior.singleagent.humanfeedback.ilearning.realtime.discounting.GeometricStep;
import burlap.behavior.singleagent.humanfeedback.tamer.TamerInfrequent;
import burlap.behavior.singleagent.humanfeedback.tamer.rewardmodel.GDHumanRewardModel;
import burlap.behavior.singleagent.learning.LearningAgent;
import burlap.behavior.singleagent.learning.tdmethods.vfa.GradientDescentSarsaLam;
import burlap.behavior.singleagent.vfa.DifferentiableStateActionValue;
import burlap.behavior.singleagent.vfa.FeatureDatabase;
import burlap.behavior.singleagent.vfa.StateToFeatureVectorGenerator;
import burlap.behavior.singleagent.vfa.common.FVToFeatureDatabase;
import burlap.behavior.singleagent.vfa.common.LinearVFA;
import burlap.behavior.singleagent.vfa.tabular.FDCombiner;
import burlap.behavior.singleagent.vfa.tabular.TabularFD;
import burlap.domain.singleagent.gridworld.GridWorldDomain;
import burlap.domain.singleagent.gridworld.GridWorldVisualizer;
import burlap.oomdp.auxiliary.StateGenerator;
import burlap.oomdp.auxiliary.common.NullTermination;
import burlap.oomdp.core.Domain;
import burlap.oomdp.core.PropositionalFunction;
import burlap.oomdp.core.objects.MutableObjectInstance;
import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.Action;
import burlap.oomdp.singleagent.GroundedAction;
import burlap.oomdp.singleagent.common.NullRewardFunction;
import burlap.oomdp.singleagent.environment.SimulatedEnvironment;
import burlap.oomdp.singleagent.environment.StateSettableEnvironment;
import burlap.oomdp.statehashing.MaskedHashableStateFactory;
import burlap.oomdp.visualizer.Visualizer;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * @author James MacGlashan.
 */
public class TrainedFourRooms {

	GridWorldDomain gwd;
	Domain domain;
	List<Action> precondActions;
	Visualizer v;
	MutableBoolean useWand = new MutableBoolean(false);
	SimulatedEnvironment env;
	FDCombiner combinedFD;

	DifferentiableStateActionValue vfa;

	FeedbackTrainingGUIConstructor guiConstructor;
	long delay = 1000;

	public TrainedFourRooms() {

		gwd = new GridWorldDomain(17, 17);

		gwd.verticalWall(0, 16, 11);
		gwd.horizontalWall(0, 16, 11);


		gwd.horizontalWall(0, 0, 5);
		gwd.horizontalWall(2, 4, 5);
		gwd.horizontalWall(6, 7, 4);
		gwd.horizontalWall(9, 10, 4);

		gwd.verticalWall(0, 0, 5);
		gwd.verticalWall(2, 7, 5);
		gwd.verticalWall(9, 10, 5);

		gwd.horizontalWall(11, 16, 9);

		gwd.verticalWall(4, 9, 13);
		gwd.setObstacleInCell(12, 4);

		this.domain = gwd.generateDomain();

		State s = GridWorldDomain.getOneAgentNoLocationState(domain, 0, 0);
		SameStateOrConstant sg = new SameStateOrConstant(s);
		env = new SimulatedEnvironment(domain, new NullRewardFunction(), new NullTermination(), sg);
		sg.env = env;

		precondActions = new ArrayList<Action>(4);
		precondActions.add(new MovementPrecondition(domain.getAction(GridWorldDomain.ACTIONNORTH), domain.getPropFunction(GridWorldDomain.PFWALLNORTH)));
		precondActions.add(new MovementPrecondition(domain.getAction(GridWorldDomain.ACTIONSOUTH), domain.getPropFunction(GridWorldDomain.PFWALLSOUTH)));
		precondActions.add(new MovementPrecondition(domain.getAction(GridWorldDomain.ACTIONEAST), domain.getPropFunction(GridWorldDomain.PFWALLEAST)));
		precondActions.add(new MovementPrecondition(domain.getAction(GridWorldDomain.ACTIONWEST), domain.getPropFunction(GridWorldDomain.PFWALLWEST)));

		v = GridWorldVisualizer.getVisualizer(gwd.getMap());


		guiConstructor = new FeedbackTrainingGUIConstructor(domain, v, env, 33);
		v.addMouseMotionListener(new MouseMotionListener() {
			@Override
			public void mouseDragged(MouseEvent e) {

			}

			@Override
			public void mouseMoved(MouseEvent e) {

				if(!useWand.b){
//					List<StateFeature> sfs = combinedFD.getStateFeatures(env.getCurrentObservation());
//					for(StateFeature sf : sfs){
//						System.out.print("(" +  sf.id + ": " + sf.value + ") ");
//					}
//					System.out.println("");
					return ;
				}

				float cWidth = v.getWidth();
				float cHeight = v.getHeight();

				int mx = e.getX();
				int my = (int)(cHeight - e.getY());

				int nxCells = 17;
				int nyCells = 17;

				int cellX = (int)(mx / (cWidth / nxCells));
				int cellY = (int)(my / (cHeight / nyCells));

				//System.out.println(cellX + " " + cellY);

				State envState = env.getCurrentObservation();
				ObjectInstance loc = envState.getFirstObjectOfClass(GridWorldDomain.CLASSLOCATION);
				if(loc == null){
					loc = new MutableObjectInstance(domain.getObjectClass(GridWorldDomain.CLASSLOCATION), "loc");
					loc.setValue(GridWorldDomain.ATTLOCTYPE, 0);
					envState.addObject(loc);
				}
				loc.setValue(GridWorldDomain.ATTX, cellX);
				loc.setValue(GridWorldDomain.ATTY, cellY);
				env.setCurStateTo(envState);


			}
		});

		v.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {

			}

			@Override
			public void keyPressed(KeyEvent e) {

			}

			@Override
			public void keyReleased(KeyEvent e) {
				if(e.getKeyChar() == 'w'){
					useWand.b = !useWand.b;
					if(!useWand.b){
						State envState = env.getCurrentObservation();
						ObjectInstance loc = envState.getFirstObjectOfClass(GridWorldDomain.CLASSLOCATION);
						while(loc != null){
							envState.removeObject(loc);
							loc = envState.getFirstObjectOfClass(GridWorldDomain.CLASSLOCATION);
						}
						env.setCurStateTo(envState);
						v.updateState(env.getCurrentObservation());
					}
				}
				else if(e.getKeyChar() == 'z'){

					State envState = env.getCurrentObservation();
					ObjectInstance agent = envState.getFirstObjectOfClass(GridWorldDomain.CLASSAGENT);
					agent.setValue(GridWorldDomain.ATTX, 14);
					agent.setValue(GridWorldDomain.ATTY, 14);
					env.setCurStateTo(envState);
					v.updateState(envState);
					env.resetEnvironment();
				}
				else if(e.getKeyChar() == 'x'){
					State envState = env.getCurrentObservation();
					ObjectInstance agent = envState.getFirstObjectOfClass(GridWorldDomain.CLASSAGENT);
					agent.setValue(GridWorldDomain.ATTX, 0);
					agent.setValue(GridWorldDomain.ATTY, 0);
					env.setCurStateTo(envState);
					v.updateState(envState);
					env.resetEnvironment();
				}
				else if(e.getKeyChar() == 'c'){
					State envState = env.getCurrentObservation();
					ObjectInstance agent = envState.getFirstObjectOfClass(GridWorldDomain.CLASSAGENT);
					agent.setValue(GridWorldDomain.ATTX, 12);
					agent.setValue(GridWorldDomain.ATTY, 10);
					env.setCurStateTo(envState);
					v.updateState(envState);
					env.resetEnvironment();
				}
				else if(e.getKeyChar() == 'v'){
					State envState = env.getCurrentObservation();
					ObjectInstance agent = envState.getFirstObjectOfClass(GridWorldDomain.CLASSAGENT);
					agent.setValue(GridWorldDomain.ATTX, 12);
					agent.setValue(GridWorldDomain.ATTY, 8);
					env.setCurStateTo(envState);
					v.updateState(envState);
					env.resetEnvironment();
				}
			}
		});

		guiConstructor.setBeginLearningKey('s');
		guiConstructor.setTerminateKey(' ');
		guiConstructor.setSendResetOnDoubleTerminate(true);
		guiConstructor.addFeedbackKey('r', 1.);
		guiConstructor.addFeedbackKey('p', -1.);
		guiConstructor.addFeedbackKey('f', 2.);
		guiConstructor.setEpisodeObserver(new EpisodeSummary());


		MaskedHashableStateFactory maskedSH = new MaskedHashableStateFactory(true, false, false, GridWorldDomain.CLASSLOCATION);
		final FeatureDatabase fd = new TabularFD(new StateEnumerator(env.getDomain(), maskedSH));
		final FVToFeatureDatabase wandFD = new FVToFeatureDatabase(new LocDirectionFV(), 4);
		combinedFD = new FDCombiner(fd, wandFD);





	}

	public void sarsa(){

		this.vfa = new LinearVFA(combinedFD, 1.);

		GradientDescentSarsaLam agent = new GradientDescentSarsaLam(env.getDomain(), 0.9, vfa, 0.1, 0.99);
		agent.setLearningPolicy(new GreedyQPolicy(agent));
		agent.setUseReplaceTraces(true);
		agent.setActions(this.precondActions);

		guiConstructor.setDelay(delay, true);
		guiConstructor.setAgent(agent);
		guiConstructor.setReceiver(guiConstructor.getHfenv());

		guiConstructor.initGUI();

	}

	public void ilearning(){

		this.vfa = new LinearVFA(combinedFD, 0.);
		GeometricStep gs = new GeometricStep(0.8);
		gs.setDiscountFactor(2., 0.95);
		ILearningRT agent = new ILearningRT(domain, gs, vfa, 1.);
		agent.setLearningPolicy(new GreedyQPolicy(agent));
		agent.setActions(this.precondActions);

		guiConstructor.setDelay(delay, true);
		guiConstructor.setAgent(agent);
		guiConstructor.setReceiver(agent);

		guiConstructor.initGUI();

	}

	public void tamer(){

		this.vfa = new LinearVFA(combinedFD, 0.);
		TamerInfrequent agent = new TamerInfrequent(domain, new GDHumanRewardModel(this.vfa, 0.1));
		agent.setActions(this.precondActions);

		guiConstructor.setDelay(delay, true);
		guiConstructor.setAgent(agent);
		guiConstructor.setReceiver(agent);

		guiConstructor.initGUI();

	}

	public static class MovementPrecondition extends AddedPreconditionAction{

		PropositionalFunction pf;

		public MovementPrecondition(Action delegate, PropositionalFunction dirWallPF) {
			super(delegate);
			this.pf = dirWallPF;
		}

		@Override
		public boolean applicableInState(State s, GroundedAction groundedAction) {
			String aname = s.getFirstObjectOfClass(GridWorldDomain.CLASSAGENT).getName();
			return !this.pf.isTrue(s, aname);
		}
	}

	public static class LocDirectionFV implements StateToFeatureVectorGenerator {

		@Override
		public double[] generateFeatureVectorFrom(State s) {

			ObjectInstance loc = s.getFirstObjectOfClass(GridWorldDomain.CLASSLOCATION);
			if(loc == null){
				return new double[4];
			}

			//otherwise find direction
			int lx = loc.getIntValForAttribute(GridWorldDomain.ATTX);
			int ly = loc.getIntValForAttribute(GridWorldDomain.ATTY);


			ObjectInstance agent = s.getFirstObjectOfClass(GridWorldDomain.CLASSAGENT);
			int ax = agent.getIntValForAttribute(GridWorldDomain.ATTX);
			int ay = agent.getIntValForAttribute(GridWorldDomain.ATTY);

			double [] vec = new double[4];

			if(ly > ay){
				vec[0] = 1.;
			}
			else if(ly < ay){
				vec[1] = 1.;
			}

			if(lx > ax){
				vec[2] = 1.;
			}
			else if(lx < ax){
				vec[3] = 1.;
			}

			return vec;
		}
	}

	public static class MutableBoolean{
		public boolean b;
		public MutableBoolean(boolean b){
			this.b = b;
		}
	}

	public static class SameStateOrConstant implements StateGenerator {
		public State constantState;
		public StateSettableEnvironment env;

		public SameStateOrConstant(State constantState) {
			this.constantState = constantState;
		}

		@Override
		public State generateState() {
			if(env == null){
				return constantState.copy();
			}
			State envState = env.getCurrentObservation();
			if(envState == null){
				return constantState.copy();
			}
			if(envState.numTotalObjects() == 0){
				return constantState.copy();
			}
			return envState;
		}
	}

	public static class EpisodeSummary implements EpisodeObserver{
		@Override
		public void learningEpisodeComplete(LearningAgent agent, EpisodeAnalysis episode) {
			System.out.println("steps/feedbacks: " + episode.maxTimeStep() + "/" + this.numFeedbacks(episode));
		}

		protected int numFeedbacks(EpisodeAnalysis ea){
			int sum = 0;
			for(int i = 1; i <= ea.maxTimeStep(); i++){
				if(ea.getReward(i) != 0.){
					sum++;
				}
			}
			return sum;
		}
	}


	public static void main(String[] args) {

		TrainedFourRooms exp = new TrainedFourRooms();
		//exp.sarsa();
		exp.ilearning();
		//exp.tamer();

	}
}
